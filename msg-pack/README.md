
# Msgpack

Simple msgpack implementation that is compatible with serde.

- Serialize Map of bool.
- Deserialize Map of bool.
- Full Deserializer
- Full Serializer
