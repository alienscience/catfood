use std::{collections::HashMap, sync::Arc};

use axum::{
    extract::Path,
    http::StatusCode,
    response::{IntoResponse, Response},
    routing::{delete, get, put},
    Json, Router,
};
use prevalence::{self, EventSource, Files, Prevalence};
use serde::{Deserialize, Serialize};
use snafu::prelude::*;

#[derive(Serialize, Deserialize)]
struct KVStore {
    lookup: HashMap<String, String>,
}

type Store = Files<KVStore>;

#[derive(Serialize, Deserialize)]
enum UpdateEvent {
    Set { key: String, value: String },
    Delete { key: String },
}

impl EventSource for KVStore {
    type Ev = UpdateEvent;
    type Res = String;

    fn update(&mut self, ev: Self::Ev) -> Result<Self::Res, prevalence::Error> {
        match ev {
            Self::Ev::Set { key, value } => {
                self.lookup.insert(key, value.clone());
                Ok(value)
            }
            Self::Ev::Delete { key } => {
                let value = self.lookup.remove(&key);
                Ok(value.unwrap_or_default())
            }
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), prevalence::Error> {
    let db = KVStore {
        lookup: HashMap::new(),
    };
    let store = Prevalence::local_files(db, "data")?;
    let store = Arc::new(store);
    let app = Router::new()
        .route(
            "/:key",
            get({
                let db = Arc::clone(&store);
                move |key| get_value(db, key)
            }),
        )
        .route(
            "/",
            put({
                let db = Arc::clone(&store);
                move |body| insert_kv(db, body)
            }),
        )
        .route(
            "/:key",
            delete({
                let db = Arc::clone(&store);
                move |key| delete_key(db, key)
            }),
        );
    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
    Ok(())
}

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("could not insert key/value"))]
    Insert { source: prevalence::Error },
    #[snafu(display("could not delete key"))]
    Delete { source: prevalence::Error },
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        (StatusCode::INTERNAL_SERVER_ERROR, self.to_string()).into_response()
    }
}

async fn get_value(db: Arc<Store>, Path(key): Path<String>) -> impl IntoResponse {
    let db = db.db();
    let mut ret = HashMap::new();
    if let Some(value) = db.lookup.get(&key).cloned() {
        ret.insert(key, value);
        (StatusCode::OK, Json(ret))
    } else {
        (StatusCode::NOT_FOUND, Json(ret))
    }
}

async fn insert_kv(
    db: Arc<Store>,
    Json(kv): Json<HashMap<String, String>>,
) -> Result<impl IntoResponse, Error> {
    for (key, value) in &kv {
        let ev = UpdateEvent::Set {
            key: key.to_owned(),
            value: value.to_owned(),
        };
        db.update(ev).context(InsertSnafu)?;
    }
    Ok((StatusCode::CREATED, Json(kv)))
}

async fn delete_key(db: Arc<Store>, Path(key): Path<String>) -> Result<impl IntoResponse, Error> {
    let ev = UpdateEvent::Delete { key: key.clone() };
    let old_value = db.update(ev).context(DeleteSnafu)?;
    let mut kv = HashMap::new();
    kv.insert(key, old_value);
    Ok((StatusCode::OK, Json(kv)))
}
