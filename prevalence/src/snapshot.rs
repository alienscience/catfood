use libc::O_DSYNC;
use serde::{de::DeserializeOwned, Serialize};
use snafu::prelude::*;
use std::{
    fs::{self, File, OpenOptions},
    io,
    marker::PhantomData,
    os::unix::fs::OpenOptionsExt,
    path::{Path, PathBuf},
    result,
};

use crate::{
    common::path_as_string,
    files::{decode_offset, snapshot_file_name, snapshot_file_offset},
};

#[derive(Debug, Snafu)]
pub enum SnapshotError {
    #[snafu(display("could not open snapshot file {path}"))]
    Open { source: io::Error, path: String },
    #[snafu(display("could not write to snapshot file at {path}"))]
    Write { source: pot::Error, path: String },
    #[snafu(display("could not deserialize snapshot"))]
    Deserialize { source: pot::Error },
    #[snafu(display("could not open snapshot directory {path}"))]
    OpenDir { source: io::Error, path: String },
    #[snafu(display("could not decode offset {offset}"))]
    DecodeOffset { offset: String },
}

pub type Result<T> = result::Result<T, SnapshotError>;

pub trait Snapshot {
    type DB;
    fn snapshot(&self, id: u128, db: &Self::DB) -> Result<()>;
    fn read_latest(&self) -> Result<Option<ReadSnapshot<Self::DB>>>;
}

pub struct ReadSnapshot<DB> {
    pub offset: u128,
    pub db: DB,
}

pub struct NoSnapshot<DB> {
    _phantom: PhantomData<DB>,
}

impl<DB> Default for NoSnapshot<DB> {
    fn default() -> Self {
        Self {
            _phantom: PhantomData::default(),
        }
    }
}

impl<DB> Snapshot for NoSnapshot<DB> {
    type DB = DB;

    fn snapshot(&self, _id: u128, _db: &Self::DB) -> Result<()> {
        Ok(())
    }

    fn read_latest(&self) -> Result<Option<ReadSnapshot<Self::DB>>> {
        Ok(None)
    }
}

pub struct FileSnapshot<DB> {
    path: PathBuf,
    _phantom: PhantomData<DB>,
}

impl<DB> FileSnapshot<DB>
where
    DB: Serialize + DeserializeOwned,
{
    pub fn new<P>(path: P) -> Self
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref().to_owned();
        Self {
            path,
            _phantom: PhantomData::default(),
        }
    }
}

impl<DB> Snapshot for FileSnapshot<DB>
where
    DB: Serialize + DeserializeOwned,
{
    type DB = DB;

    fn snapshot(&self, id: u128, db: &Self::DB) -> Result<()> {
        let file_name = snapshot_file_name(id);
        let mut path = PathBuf::from(&self.path);
        path.push(file_name);
        let f = OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .custom_flags(O_DSYNC)
            .open(&path)
            .with_context(|_| OpenSnafu {
                path: path_as_string(&path),
            })?;
        pot::to_writer(db, &f).with_context(|_| WriteSnafu {
            path: path_as_string(&path),
        })?;
        Ok(())
    }

    fn read_latest(&self) -> Result<Option<ReadSnapshot<Self::DB>>> {
        let files = snapshot_files(&self.path)?;
        let Some((path, offset)) = files.into_iter().max_by(|(_, o1), (_, o2)| o1.cmp(o2)) else {
            return Ok(None);
        };
        let offset =
            decode_offset(&offset).ok_or_else(|| SnapshotError::DecodeOffset { offset })?;
        let file = File::open(&path).with_context(|_| OpenSnafu {
            path: path_as_string(&path),
        })?;
        let db: Self::DB = pot::from_reader(&file).context(DeserializeSnafu)?;
        Ok(Some(ReadSnapshot { offset, db }))
    }
}

fn snapshot_files(path: &Path) -> Result<Vec<(PathBuf, String)>> {
    let files = fs::read_dir(&path).with_context(|_| OpenDirSnafu {
        path: path_as_string(&path),
    })?;
    let files: result::Result<Vec<_>, _> = files.map(|res| res.map(|d| d.path())).collect();
    let files = files.with_context(|_| OpenDirSnafu {
        path: path_as_string(&path),
    })?;
    let snapshot_files = files
        .into_iter()
        .filter_map(|p| snapshot_file_offset(&p).map(|o| (p, o)))
        .collect();
    Ok(snapshot_files)
}
