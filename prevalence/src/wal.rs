use libc::O_DSYNC;
use parking_lot::Mutex;
use snafu::prelude::*;
use std::{
    fs::{self, File, OpenOptions},
    io::{self, ErrorKind},
    iter,
    marker::PhantomData,
    os::unix::fs::OpenOptionsExt,
    path::{Path, PathBuf},
    result,
};

use crate::{
    common::path_as_string,
    files::{encode_offset, wal_file_name, wal_file_offset},
    EventSource,
};

#[derive(Debug, Snafu)]
pub enum WalError {
    #[snafu(display("could not open WAL file {path}"))]
    Open { source: io::Error, path: String },
    #[snafu(display("could not open WAL directory {path}"))]
    OpenDir { source: io::Error, path: String },
    #[snafu(display("could not lock WAL mutex"))]
    Lock,
    #[snafu(display("could not deserialize event"))]
    Deserialize { source: pot::Error },
    #[snafu(display("could not update WAL"))]
    Update { source: pot::Error },
    #[snafu(display("WAL used before rotate"))]
    NeedsRotate,
}

pub type Result<T> = result::Result<T, WalError>;
/// Can only return Box<Iterator> from traits.
pub type DynIter<Ev> = Box<dyn Iterator<Item = Result<Ev>>>;

pub trait Wal {
    type DB: EventSource;

    fn update(&self, ev: &<Self::DB as EventSource>::Ev) -> Result<()>;
    fn load(&self, offset: u128) -> Result<DynIter<<Self::DB as EventSource>::Ev>>;
    fn rotate(&self, offset: u128) -> Result<()>;
}

pub struct FileWal<DB>
where
    DB: EventSource,
{
    inner: Mutex<ProtectedWal<DB>>,
}

pub struct FileWalBuilder {
    path: PathBuf,
}

impl FileWalBuilder {
    pub fn new<P>(path: P) -> Self
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref().to_owned();
        Self { path }
    }

    pub fn build<DB>(self) -> Result<FileWal<DB>>
    where
        DB: EventSource,
    {
        FileWal::new(self.path)
    }
}

struct ProtectedWal<DB>
where
    DB: EventSource,
{
    path: PathBuf,
    current: Option<File>,
    _phantom: PhantomData<DB>,
}

impl<DB> FileWal<DB>
where
    DB: EventSource,
{
    pub fn new<P>(path: P) -> Result<Self>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref().to_owned();
        let inner = ProtectedWal::new(path)?;
        Ok(Self {
            inner: Mutex::new(inner),
        })
    }
}

impl<DB> Wal for FileWal<DB>
where
    DB: EventSource,
{
    type DB = DB;

    fn update(&self, ev: &DB::Ev) -> Result<()> {
        let mut inner = self.inner.lock();
        inner.update(ev)
    }

    fn load(&self, offset: u128) -> Result<DynIter<DB::Ev>> {
        let inner = self.inner.lock();
        inner.load(offset)
    }

    fn rotate(&self, offset: u128) -> Result<()> {
        let mut inner = self.inner.lock();
        inner.rotate(offset)
    }
}

// Convert a result to a singled valued iterator containing an error
macro_rules! iter_err {
    ($x:expr) => {
        match $x {
            Ok(t) => t,
            Err(e) => return Box::new(iter::once(Err(e))),
        }
    };
}

impl<DB> ProtectedWal<DB>
where
    DB: EventSource,
{
    fn new(path: PathBuf) -> Result<Self> {
        Ok(Self {
            path,
            current: None,
            _phantom: Default::default(),
        })
    }

    fn update(&mut self, ev: &<DB as EventSource>::Ev) -> Result<()> {
        let current = self.current.as_mut().ok_or_else(|| WalError::NeedsRotate)?;
        pot::to_writer(&ev, current).context(UpdateSnafu)?;
        Ok(())
    }

    fn load(&self, offset: u128) -> Result<DynIter<DB::Ev>> {
        let wal_files = iter_wal_files(&self.path, offset)?;
        Ok(Box::new(wal_files.flat_map(|p| Self::read_wal_file(&p))))
    }

    fn rotate(&mut self, wal_events: u128) -> Result<()> {
        self.current = Some(Self::open_write_wal(&self.path, wal_events)?);
        Ok(())
    }

    fn open_write_wal(path: &Path, offset: u128) -> Result<File> {
        let file_name = wal_file_name(offset);
        let mut path = PathBuf::from(path);
        path.push(file_name);
        let file = OpenOptions::new()
            .append(true)
            .create(true)
            .custom_flags(O_DSYNC)
            .open(&path)
            .with_context(|_| OpenSnafu {
                path: path_as_string(&path),
            })?;
        Ok(file)
    }

    fn read_wal_file(file: &Path) -> DynIter<DB::Ev> {
        let file = File::open(file).with_context(|_| OpenSnafu {
            path: path_as_string(file),
        });
        let file = iter_err!(file);
        let stream = iter::from_fn(move || {
            let res = pot::from_reader(&file);
            match res {
                Err(pot::Error::Eof) => None,
                Err(pot::Error::Io(e)) if e.kind() == ErrorKind::UnexpectedEof => None,
                _ => Some(res.context(DeserializeSnafu)),
            }
        });
        Box::new(stream)
    }
}

fn iter_wal_files(path: &Path, offset: u128) -> Result<impl Iterator<Item = PathBuf>> {
    let files = fs::read_dir(&path).with_context(|_| OpenDirSnafu {
        path: path_as_string(&path),
    })?;
    // Exit early if any DirEntry has an error.
    let files: result::Result<Vec<_>, _> = files.map(|res| res.map(|d| d.path())).collect();
    let files = files.with_context(|_| OpenDirSnafu {
        path: path_as_string(&path),
    })?;
    Ok(wal_files_from_offset(files, offset))
}

fn wal_files_from_offset(paths: Vec<PathBuf>, offset: u128) -> impl Iterator<Item = PathBuf> {
    // Sort the wal filenames
    let mut files: Vec<_> = paths
        .into_iter()
        .filter_map(|p| wal_file_offset(&p).map(|o| (p, o)))
        .collect();
    files.sort_unstable_by(|(_, o1), (_, o2)| o1.cmp(o2));
    // Iterate starting at the given offsets
    let offset = encode_offset(offset);
    files
        .into_iter()
        .skip_while(move |(_, o)| o != &offset)
        .map(|(p, _)| p)
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use super::wal_files_from_offset;

    #[test]
    fn iter_wal_file_offset() {
        let files = vec![
            "wal-0000000000000000000008VVVG.pot",
            "random-file.txt",
            "wal-00000000000000000000007VVS.pot",
            "wal-00000000000000000000000000.pot",
        ];
        let files: Vec<_> = files.into_iter().map(PathBuf::from).collect();
        let from_offset: Vec<_> = wal_files_from_offset(files, 0xffff).collect();
        assert_eq!(
            from_offset[0],
            PathBuf::from("wal-00000000000000000000007VVS.pot")
        );
        assert_eq!(
            from_offset[1],
            PathBuf::from("wal-0000000000000000000008VVVG.pot")
        );
    }
}
