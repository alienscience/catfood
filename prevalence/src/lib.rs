mod common;
mod files;
mod prevalence;
mod snapshot;
mod wal;

pub use crate::{
    prevalence::{Error, EventSource, Files, Prevalence, PrevalenceBuilder},
    snapshot::{FileSnapshot, Snapshot},
    wal::{FileWal, FileWalBuilder, Wal},
};

#[cfg(test)]
mod tests {
    use crate::{prevalence::Error, snapshot::NoSnapshot};

    use super::*;
    use serde::{Deserialize, Serialize};
    use std::{fs, ops::Deref};
    use temp_dir::TempDir;

    #[derive(Serialize, Deserialize)]
    #[serde(tag = "type")]
    enum SimpleDB {
        V1 { count: u32 },
    }

    #[derive(Serialize, Deserialize)]
    #[serde(tag = "type")]
    enum SimpleEv {
        IncrementV1,
        DecrementV1,
    }

    impl EventSource for SimpleDB {
        type Ev = SimpleEv;
        type Res = u32;

        fn update(&mut self, _ev: Self::Ev) -> Result<Self::Res, Error> {
            match self {
                Self::V1 { count } => {
                    *count += 1;
                    Ok(*count)
                }
            }
        }
    }

    #[test]
    fn construct() {
        let dir = TempDir::new().unwrap();
        let path = dir.path();
        let empty_db = SimpleDB::V1 { count: 0 };
        let wal = FileWalBuilder::new(path).build().unwrap();
        let snapshot = FileSnapshot::new(path);
        let p = PrevalenceBuilder::default()
            .db(empty_db)
            .wal(wal)
            .snapshot(snapshot)
            .rotate_events(2)
            .build()
            .unwrap();
        let mut res = p.update(SimpleEv::IncrementV1).unwrap();
        assert_eq!(res, 1);
        res = p.update(SimpleEv::IncrementV1).unwrap();
        assert_eq!(res, 2);
        res = p.update(SimpleEv::IncrementV1).unwrap();
        assert_eq!(res, 3);
        assert!(matches!(p.db().deref(), SimpleDB::V1 { count: 3 }));
    }

    #[test]
    fn easy_construct() {
        let dir = TempDir::new().unwrap();
        let empty_db = SimpleDB::V1 { count: 0 };
        let p = Prevalence::local_files(empty_db, dir.path()).unwrap();
        p.update(SimpleEv::IncrementV1).unwrap();
        p.update(SimpleEv::IncrementV1).unwrap();
        p.update(SimpleEv::IncrementV1).unwrap();
        assert!(matches!(p.db().deref(), SimpleDB::V1 { count: 3 }));
    }

    #[test]
    fn restore_from_wal() {
        let dir = TempDir::new().unwrap();
        let path = dir.path();
        // Create WAL and Snapshot
        {
            let empty_db = SimpleDB::V1 { count: 0 };
            let wal = FileWalBuilder::new(path).build().unwrap();
            let snapshot = NoSnapshot::default();
            let p = PrevalenceBuilder::default()
                .db(empty_db)
                .wal(wal)
                .snapshot(snapshot)
                .rotate_events(2)
                .build()
                .unwrap();
            for _ in 0..8 {
                p.update(SimpleEv::IncrementV1).unwrap();
            }
            assert!(matches!(p.db().deref(), SimpleDB::V1 { count: 8 }));
        }
        // Restore from WAL and Snapshot
        let empty_db = SimpleDB::V1 { count: 0 };
        let wal = FileWalBuilder::new(path).build().unwrap();
        let snapshot = FileSnapshot::new(path);
        let p = PrevalenceBuilder::default()
            .db(empty_db)
            .wal(wal)
            .snapshot(snapshot)
            .rotate_events(2)
            .build()
            .unwrap();
        assert!(matches!(p.db().deref(), SimpleDB::V1 { count: 8 }));
    }

    #[test]
    fn restore_from_snapshot() {
        let dir = TempDir::new().unwrap();
        let path = dir.path();
        // Create WAL and Snapshot
        {
            let empty_db = SimpleDB::V1 { count: 0 };
            let wal = FileWalBuilder::new(path).build().unwrap();
            let snapshot = FileSnapshot::new(path);
            let p = PrevalenceBuilder::default()
                .db(empty_db)
                .wal(wal)
                .snapshot(snapshot)
                .rotate_events(2)
                .build()
                .unwrap();
            for _ in 0..9 {
                p.update(SimpleEv::IncrementV1).unwrap();
            }
        }
        // Delete all WAL files except the last one.
        let files = fs::read_dir(&path).unwrap();
        let mut wal_files: Vec<_> = files
            .into_iter()
            .map(|d| d.unwrap())
            .filter(|d| {
                d.file_name()
                    .to_str()
                    .filter(|s| s.starts_with("wal"))
                    .is_some()
            })
            .filter_map(|d| d.path().to_str().map(String::from))
            .collect();
        wal_files.sort_unstable();
        let Some(_remaining_wal) = wal_files.pop() else {
            panic!("No wal files")
        };
        for file in wal_files {
            fs::remove_file(&file).unwrap();
        }
        // Restore from the snapshot and the remaining WAL
        let empty_db = SimpleDB::V1 { count: 0 };
        let wal = FileWalBuilder::new(path).build().unwrap();
        let snapshot = FileSnapshot::new(path);
        let p = PrevalenceBuilder::default()
            .db(empty_db)
            .wal(wal)
            .snapshot(snapshot)
            .rotate_events(2)
            .build()
            .unwrap();
        assert!(matches!(p.db().deref(), SimpleDB::V1 { count: 9 }));
    }
}
