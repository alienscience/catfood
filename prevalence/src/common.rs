use std::path::Path;

pub fn path_as_string(path: &Path) -> String {
    format!("{}", path.display())
}
