use std::{
    ops::DerefMut,
    path::Path,
    time::{Duration, Instant},
};

use serde::{de::DeserializeOwned, Serialize};
use snafu::prelude::*;

use crate::{snapshot::SnapshotError, wal::WalError, FileSnapshot, FileWal, Snapshot, Wal};

#[derive(Debug, Snafu)]
pub enum Error {
    #[snafu(display("could not build Prevalence, missing fields"))]
    Build,
    #[snafu(display("could not create snapshot"))]
    SnapshotCreate { source: SnapshotError },
    #[snafu(display("could not create WAL"))]
    WalCreate { source: WalError },
    #[snafu(display("could not update WAL"))]
    WalUpdate { source: WalError },
    #[snafu(display("could not sync from WAL"))]
    WalSync { source: WalError },
    #[snafu(display("could not write snapshot"))]
    SnapshotWrite { source: SnapshotError },
    #[snafu(display("could not sync from snapshot"))]
    SnapshotSync { source: SnapshotError },
    #[snafu(display("could not update DB"))]
    DBUpdate { source: Box<dyn std::error::Error> },
}

type RwLock<DB> = parking_lot::RwLock<ProtectedPrevalence<DB>>;
type ReadLockGuard<'a, DB> = parking_lot::MappedRwLockReadGuard<'a, DB>;

pub const DEFAULT_ROTATE_EVENTS: u128 = 2_u128.pow(20);
pub const DEFAULT_ROTATE_PERIOD: Duration = Duration::from_secs(24 * 3600);

pub struct Prevalence<DB, WAL, SNAP>
where
    DB: EventSource + Serialize + DeserializeOwned,
    WAL: Wal<DB = DB>,
    SNAP: Snapshot<DB = DB>,
{
    wal: WAL,
    snapshot: SNAP,
    protected: RwLock<DB>,
    rotate_events: u128,
    rotate_period: Duration,
}

struct ProtectedPrevalence<DB>
where
    DB: EventSource + Serialize + DeserializeOwned,
{
    db: DB,
    num_events: u128,
    last_rotate: Instant,
}

pub trait EventSource: Serialize + DeserializeOwned {
    type Ev: Serialize + DeserializeOwned + 'static;
    type Res;

    fn update(&mut self, ev: Self::Ev) -> Result<Self::Res, Error>;
}

impl<DB, WAL, SNAP> Prevalence<DB, WAL, SNAP>
where
    DB: EventSource + Serialize + DeserializeOwned,
    WAL: Wal<DB = DB>,
    SNAP: Snapshot<DB = DB>,
{
    pub fn update(&self, ev: DB::Ev) -> Result<DB::Res, Error> {
        self.wal.update(&ev).context(WalUpdateSnafu)?;
        let mut protected = self.protected.write();
        protected.num_events += 1;
        let num_events = protected.num_events;
        let last_rotate = protected.last_rotate;
        let inner = protected.deref_mut();
        let db = &mut inner.db;
        let ret = db.update(ev)?;
        if num_events % self.rotate_events == 0 || Instant::now() - last_rotate > self.rotate_period
        {
            self.wal.rotate(num_events).context(WalUpdateSnafu)?;
            self.snapshot
                .snapshot(num_events, db)
                .context(SnapshotWriteSnafu)?;
            inner.last_rotate = Instant::now();
        }
        Ok(ret)
    }

    pub fn db(&self) -> ReadLockGuard<DB> {
        parking_lot::RwLockReadGuard::<'_, ProtectedPrevalence<DB>>::map(
            self.protected.read(),
            |p| &p.db,
        )
    }
}

/// A database that uses a file snapshot and a file WAL.
pub type Files<DB> = Prevalence<DB, FileWal<DB>, FileSnapshot<DB>>;

impl<DB> Files<DB>
where
    DB: EventSource,
{
    pub fn local_files<P>(db: DB, path: P) -> Result<Self, Error>
    where
        P: AsRef<Path>,
    {
        let path = path.as_ref();
        let snapshot = FileSnapshot::new(path);
        let wal = FileWal::new(path).context(WalCreateSnafu)?;
        PrevalenceBuilder::default()
            .db(db)
            .wal(wal)
            .snapshot(snapshot)
            .build()
    }
}

pub struct PrevalenceBuilder<DB, WAL, SNAP>
where
    DB: EventSource,
    WAL: Wal<DB = DB>,
    SNAP: Snapshot<DB = DB>,
{
    db: Option<DB>,
    wal: Option<WAL>,
    snapshot: Option<SNAP>,
    rotate_events: u128,
    rotate_period: Duration,
}

impl<DB, WAL, SNAP> PrevalenceBuilder<DB, WAL, SNAP>
where
    DB: EventSource,
    WAL: Wal<DB = DB>,
    SNAP: Snapshot<DB = DB>,
{
    pub fn db(&mut self, db: DB) -> &mut Self {
        self.db = Some(db);
        self
    }

    pub fn wal(&mut self, wal: WAL) -> &mut Self {
        self.wal = Some(wal);
        self
    }

    pub fn snapshot(&mut self, snapshot: SNAP) -> &mut Self {
        self.snapshot = Some(snapshot);
        self
    }

    pub fn rotate_events(&mut self, num_events: u128) -> &mut Self {
        self.rotate_events = num_events;
        self
    }

    pub fn rotate_period(&mut self, period: Duration) -> &mut Self {
        self.rotate_period = period;
        self
    }

    pub fn build(&mut self) -> Result<Prevalence<DB, WAL, SNAP>, Error> {
        let snapshot = self.snapshot.take().ok_or(Error::Build)?;
        let wal = self.wal.take().ok_or(Error::Build)?;
        let (mut db, mut offset) =
            if let Some(snapshot_db) = snapshot.read_latest().context(SnapshotSyncSnafu)? {
                (snapshot_db.db, snapshot_db.offset)
            } else {
                let db = self.db.take().ok_or(Error::Build)?;
                (db, 0)
            };
        for ev in wal.load(offset).context(WalSyncSnafu)? {
            let ev = ev.context(WalSyncSnafu)?;
            db.update(ev)?;
            offset += 1;
        }
        wal.rotate(offset).context(WalSyncSnafu)?;
        let protected = parking_lot::RwLock::new(ProtectedPrevalence {
            db,
            num_events: offset,
            last_rotate: Instant::now(),
        });
        Ok(Prevalence {
            wal,
            snapshot,
            protected,
            rotate_events: self.rotate_events,
            rotate_period: self.rotate_period,
        })
    }
}

impl<DB, WAL, SNAP> Default for PrevalenceBuilder<DB, WAL, SNAP>
where
    DB: EventSource,
    WAL: Wal<DB = DB>,
    SNAP: Snapshot<DB = DB>,
{
    fn default() -> Self {
        Self {
            db: None,
            wal: None,
            snapshot: None,
            rotate_events: DEFAULT_ROTATE_EVENTS,
            rotate_period: DEFAULT_ROTATE_PERIOD,
        }
    }
}
