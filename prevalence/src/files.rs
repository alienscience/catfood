use std::{path::Path, sync::OnceLock};

use data_encoding::BASE32HEX_NOPAD;
use regex::Regex;

const WAL_FILE_PREFIX: &str = "wal";
const WAL_FILE_SUFFIX: &str = "pot";
const SNAPSHOT_FILE_PREFIX: &str = "snap";
const SNAPSHOT_FILE_SUFFIX: &str = "pot";

pub fn encode_offset(offset: u128) -> String {
    BASE32HEX_NOPAD.encode(&offset.to_be_bytes())
}

pub fn decode_offset(offset: &str) -> Option<u128> {
    BASE32HEX_NOPAD
        .decode(offset.as_bytes())
        .ok()
        .and_then(vec_to_u128)
}

pub fn wal_file_name(offset: u128) -> String {
    let offset = encode_offset(offset);
    format!("{WAL_FILE_PREFIX}-{offset}.{WAL_FILE_SUFFIX}")
}

pub fn snapshot_file_name(offset: u128) -> String {
    let offset = encode_offset(offset);
    format!("{SNAPSHOT_FILE_PREFIX}-{offset}.{SNAPSHOT_FILE_SUFFIX}")
}

pub fn wal_file_offset(p: &Path) -> Option<String> {
    let wal_file = p.file_name()?.to_str()?;
    let re = wal_file_regex();
    re.captures(wal_file).map(|cap| cap[1].to_string())
}

pub fn snapshot_file_offset(p: &Path) -> Option<String> {
    let snapshot_file = p.file_name()?.to_str()?;
    let re = snapshot_file_regex();
    re.captures(snapshot_file).map(|cap| cap[1].to_string())
}

fn wal_file_regex<'a>() -> &'a Regex {
    static RE: OnceLock<Regex> = OnceLock::new();
    RE.get_or_init(|| {
        let re = format!("^{WAL_FILE_PREFIX}-([0-9A-V]{{26}}).{WAL_FILE_SUFFIX}$");
        Regex::new(&re).unwrap_or_else(|e| panic!("WAL regex `{re}` compile failure: {e}"))
    })
}

fn snapshot_file_regex<'a>() -> &'a Regex {
    static RE: OnceLock<Regex> = OnceLock::new();
    RE.get_or_init(|| {
        let re = format!("^{SNAPSHOT_FILE_PREFIX}-([0-9A-V]{{26}}).{SNAPSHOT_FILE_SUFFIX}$");
        Regex::new(&re).unwrap_or_else(|e| panic!("Snapshot regex `{re}` compile failure: {e}"))
    })
}

fn vec_to_u128(v: Vec<u8>) -> Option<u128> {
    let arr: [u8; 16] = v.try_into().ok()?;
    Some(u128::from_be_bytes(arr))
}

#[cfg(test)]
mod tests {
    use super::{decode_offset, encode_offset, wal_file_name};

    #[test]
    fn encode_offsets() {
        assert_eq!(encode_offset(0x0000), "00000000000000000000000000");
        assert_eq!(encode_offset(0xffff), "00000000000000000000007VVS");
        assert_eq!(
            encode_offset(0xffffffff_ffffffff_ffffffff_ffffffff),
            "VVVVVVVVVVVVVVVVVVVVVVVVVS"
        );
    }

    #[test]
    fn file_names() {
        assert_eq!(wal_file_name(0x0000), "wal-00000000000000000000000000.pot");
        assert_eq!(wal_file_name(0xffff), "wal-00000000000000000000007VVS.pot");
        assert_eq!(
            wal_file_name(0xffffffff_ffffffff_ffffffff_ffffffff),
            "wal-VVVVVVVVVVVVVVVVVVVVVVVVVS.pot"
        );
    }

    #[test]
    fn offset_decodes() {
        assert_eq!(decode_offset("00000000000000000000000000"), Some(0x0000));
        assert_eq!(decode_offset("00000000000000000000007VVS"), Some(0xffff));
        assert_eq!(
            decode_offset("VVVVVVVVVVVVVVVVVVVVVVVVVS"),
            Some(0xffffffff_ffffffff_ffffffff_ffffffff),
        );
    }
}
