
# Possible Repo Setup

- Gitea/Forgejo on Hetzner (fetti?)
    - Forgejo is possibly nicer.
    - Data stored on external volume
    - sqlite
- Use Gitea/Forgejo Actions
    - Runner on Contabo (4 CPU, 8GB, €8pm).
        - Cannot run VMs on demand, must be monthly.
    - Rocky linux (rolling distribution)
    - Docker must be installed
    - No packer support, must be updated manually?
        - Maybe use [dnf-automatic](https://dnf.readthedocs.io/en/latest/automatic.html).
