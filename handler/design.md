
```mermaid
classDiagram
    class Server {
        <<Send + Sync>>
        +set_handler(Handler)
    }
    class Session {
        <<Send + Sync>>
        +process(Stuff)
    }
    class Handler {
        <<singleton + Send + Sync>>
    }
    Server o-- Handler
    Server ..> Session : creates
    Session o-- Handler
```