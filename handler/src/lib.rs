use std::sync::Arc;

pub struct Server {
    handler: Arc<dyn Handler>,
}

pub struct Session {
    handler: Arc<dyn Handler>,
}

pub trait Handler: Send + Sync {
    fn increment(&self);
    fn decrement(&self);
    fn value(&self) -> u32;
}

impl Server {
    pub fn new(handler: Arc<dyn Handler>) -> Self {
        Self { handler }
    }

    pub fn session(&self) -> Session {
        Session {
            handler: self.handler.clone(),
        }
    }
}

impl Session {
    pub fn process(&self) {
        self.handler.increment();
    }
}

#[cfg(test)]
mod tests {
    use std::{
        sync::atomic::{AtomicU32, Ordering},
        thread,
    };

    use super::*;

    // TODO: aync test, maybe async
    struct MyHandler {
        counter: AtomicU32,
    }

    impl Handler for MyHandler {
        fn increment(&self) {
            self.counter.fetch_add(1, Ordering::Relaxed);
        }

        fn decrement(&self) {
            self.counter.fetch_sub(1, Ordering::Relaxed);
        }

        fn value(&self) -> u32 {
            self.counter.load(Ordering::Relaxed)
        }
    }

    #[test]
    fn with_handler() {
        let handler = Arc::new(MyHandler {
            counter: AtomicU32::new(0),
        });
        let server = Server::new(handler);
        thread::spawn(move || {
            for _ in 0..4 {
                let session = server.session();
                thread::spawn(move || {
                    session.process();
                    session.process();
                    assert!(session.handler.value() >= 2);
                })
                .join()
                .expect("Background thread failed");
            }
        })
        .join()
        .expect("Main thread failed");
    }
}
