package main

import (
	"flag"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/rjeczalik/notify"
)

const (
	IncomingDir    = "inbox"
	ChecksumSuffix = ".sha256sum"
)

func main() {
	log.SetFlags(0)
	dryrun := flag.Bool("dryrun", false, "Do not deploy, just print the command")
	flag.Parse()
	ch := make(chan notify.EventInfo, 1)
	debounce := make(map[string]time.Time)
	err := notify.Watch(IncomingDir, ch, notify.Write)
	if err != nil {
		log.Fatal(err)
	}
	defer notify.Stop(ch)
	for ev := range ch {
		path := ev.Path()
		if !strings.HasSuffix(path, ChecksumSuffix) {
			continue
		}
		last := debounce[path]
		if time.Since(last) < 30*time.Second {
			continue
		}
		debounce[path] = time.Now()
		go deploy(path, *dryrun)
	}
}

func deploy(path string, dryrun bool) {
	rpm := strings.TrimSuffix(path, ChecksumSuffix)
	var cmd *exec.Cmd
	if dryrun {
		cmd = exec.Command("echo", "rpm", "-iv", "--force", rpm)
	} else {
		cmd = exec.Command("sudo", "rpm", "-iv", "--force", rpm)
	}
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	err := cmd.Run()
	if err != nil {
		log.Println("Err:", err)
	}
}
