
# Notes

n - length of ngram (block)
N - Number of ngrams
B - Number of Buckets
h - hash function (after address conversion)
H - Recursive Hash function
r - radix (some constant), part of Ring R
T - Transformation function of symbols to R
x - Polynomial coeffients, every bit of the is a coefficient.

## Operations

On GF(2), cyclic poly:
- Add is XOR
- x^delta * q = BROTL(q, delta)
- Addition and subtraction are the same.

## Ring Field Domain

Ring -  Add and Multiply but can be e.g non-communative, without inverse
Field - Add, Subtract and Multiply behave like real numbers
Domain - Ring where ab=0 implies a=0 or b=0
Integral Domain - Communative domain
Galois Field - Field with finite number of elements (2 in this paper)

## Questions

- Why does an Integral Domain need a prime number for hashing?
- Is algorithm in Section 2.4 decomposable?
- Is algorithm in Section 2.5 valid for n > 32?