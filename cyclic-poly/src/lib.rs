use pcg_rand::Pcg32;
use rand::prelude::*;

pub struct CyclicPoly {
    current: u64,
    transformation: [u64; 256],
    reverse: [u64; 256],
}

/// Algorithm described in Section 2.3
impl CyclicPoly {
    pub fn new(block: &[u8]) -> Self {
        let n: u32 = block.len().try_into().unwrap();
        let mut ret = Self {
            current: 0,
            transformation: [0; 256],
            reverse: [0; 256],
        };
        let mut rng = Pcg32::seed_from_u64(1337);
        for i in 0..ret.transformation.len() {
            ret.transformation[i] = rng.gen();
        }
        for i in 0..ret.reverse.len() {
            ret.reverse[i] = ret.transformation[i].rotate_left(n);
        }
        ret.from_block(block);
        ret
    }

    pub fn value(&self) -> u64 {
        self.current
    }

    pub fn rotate(&mut self, incoming: u8, outgoing: u8) {
        let t_in = self.transformation[usize::from(incoming)];
        let t_out = self.reverse[usize::from(outgoing)];
        let current = self.current.rotate_left(1);
        let current = current ^ t_in;
        self.current = current ^ t_out;
    }

    fn from_block(&mut self, block: &[u8]) {
        for v in block.iter() {
            let current = self.current.rotate_left(1);
            let b = usize::from(*v);
            let t = self.transformation[b];
            self.current = current ^ t;
        }
    }

    pub fn decompose_second(parent: u64, first_child: u64, second_block_size: u32) -> u64 {
        let first_brotl = first_child.rotate_left(second_block_size);
        parent ^ first_brotl
    }
}

#[cfg(test)]
mod tests {
    use super::CyclicPoly;
    use quickcheck_macros::quickcheck;

    #[quickcheck]
    fn sensitivity(data0: Vec<u8>, i: usize) -> bool {
        if data0.is_empty() {
            return true;
        }
        let mut data1 = data0.clone();
        data1[i % data0.len()] ^= 0xaa;
        let hash0 = CyclicPoly::new(&data0).value();
        let hash1 = CyclicPoly::new(&data1).value();
        hash0 != hash1
    }

    #[quickcheck]
    fn roll(data: Vec<u8>) -> bool {
        if data.len() == 1 {
            return true;
        }
        let data = data.as_slice();
        let data_len = data.len();
        let n = data_len / 2;
        let last_block = &data[(data_len - n)..data_len];
        let full = CyclicPoly::new(last_block).value();
        let mut rolling = CyclicPoly::new(&data[0..n]);
        for i in n..data_len {
            rolling.rotate(data[i], data[i - n])
        }
        full == rolling.value()
    }

    #[quickcheck]
    fn decompose(data: Vec<u8>) -> bool {
        if data.len() == 1 {
            return true;
        }
        let data = data.as_slice();
        let data_len = data.len();
        let n = data_len / 2;
        let first_block = &data[0..(data_len - n)];
        let second_block = &data[(data_len - n)..data_len];
        let parent = CyclicPoly::new(data).value();
        let first = CyclicPoly::new(first_block).value();
        let second = CyclicPoly::new(second_block).value();
        second == CyclicPoly::decompose_second(parent, first, second_block.len() as u32)
    }
}
