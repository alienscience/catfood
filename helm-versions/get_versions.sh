#!/bin/bash

function version {
	curl -s "https://artifacthub.io/api/v1/packages/helm/$1" \
		-H 'accept: application/json' | \
		jq '.version'
}

version "forgejo-helm/forgejo"
version "woodpecker-ci/woodpecker"
