use std::{error::Error, io};

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "PascalCase")]
struct RawTitanic {
    passenger_id: u16,
    survived: u8,
    pclass: u8,
    name: String,
    sex: String,
    age: Option<f32>,
    sib_sp: u8,
    parch: u8,
    ticket: String,
    fare: f32,
    cabin: Option<String>,
    embarked: Option<char>,
}

// TODO: check fastai course
struct NormalisedTitanic {
    passenger_id: u16,
    survived: u8,
    pclass: u8,
    name: String,
    sex: String,
    age: Option<f32>,
    sib_sp: u8,
    parch: u8,
    ticket: String,
    fare: f32,
    cabin: Option<String>,
    embarked: Option<char>,
}

fn main() -> Result<(), Box<dyn Error>> {
    let mut rdr = csv::Reader::from_reader(io::stdin());
    for result in rdr.deserialize() {
        let record: RawTitanic = result?;
        println!("{:?}", record);
    }
    Ok(())
}
