package main

import (
	"fmt"
	"io"
)

type RemoteDest struct {
	link *Channel
}

func newRemoteDest(link io.ReadWriteCloser) (*RemoteDest, error) {
	channel := newChannel(link)
	d := &RemoteDest{channel}
	return d, d.waitForReady()
}

func (d *RemoteDest) waitForReady() error {
	var ready MessageHeader
	err := d.link.Receive(&ready)
	if err != nil {
		return err
	}
	if ready.ID != ReadyID {
		return fmt.Errorf("unexpected message ID %d", ready.ID)
	}
	return nil
}

func (d *RemoteDest) TransferFile(src, dest string) error {
	transfer := TransferFile{src, dest}
	return d.send(TransferFileID, transfer)
}

func (d *RemoteDest) FileAppend(buf []byte) error {
	chunk := TransferChunk{buf}
	return d.send(TransferChunkID, chunk)
}

func (d *RemoteDest) FileClose() error {
	err := d.send(TransferEndID, nil)
	if err != nil {
		return fmt.Errorf("cannot end file transfer: %w", err)
	}
	return d.waitForReady()
}

func (d *RemoteDest) End() error {
	return d.send(EndID, nil)
}

func (d *RemoteDest) send(ID int, value interface{}) error {
	msgHeader := MessageHeader{ID: ID}
	err := d.link.Send(&msgHeader)
	if err != nil {
		return err
	}
	if value == nil {
		return nil
	}
	return d.link.Send(value)
}
