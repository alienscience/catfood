package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

type ServerDest struct {
	link          *Channel
	subscriptions *Subscriptions
	file          *os.File
	end           chan bool
}

const (
	ReadyID = iota
	TransferChunkID
	TransferFileID
	TransferEndID
	EndID
)

type MessageHeader struct {
	ID int
}

type TransferChunk struct {
	Buf []byte
}
type TransferFile struct {
	Src  string
	Dest string
}

var l = log.New(os.Stderr, "serve: ", 0)

func newServerDest(link io.ReadWriteCloser) *ServerDest {
	channel := newChannel(link)
	subs := NewSubscriptions(channel)
	s := &ServerDest{
		link:          channel,
		subscriptions: subs,
		file:          nil,
		end:           make(chan bool),
	}
	s.setupHandlers()
	return s
}

func (s *ServerDest) setupHandlers() {
	s.subscriptions.Add(TransferFileID, TransferFile{}, s.transferFile)
	s.subscriptions.Add(TransferChunkID, TransferChunk{}, s.fileAppend)
	s.subscriptions.Add(TransferEndID, nil, s.fileClose)
	s.subscriptions.Add(EndID, nil, s.shutdown)
}

func (s *ServerDest) Serve() error {
	defer s.link.Close()
	err := s.link.Send(MessageHeader{ID: ReadyID})
	if err != nil {
		return err
	}
	return s.subscriptions.Run()
}

func (s *ServerDest) transferFile(args interface{}) error {
	transfer, ok := args.(TransferFile)
	if !ok {
		return fmt.Errorf("Unexpected type to transferFile: %v", args)
	}
	if isDirectory(transfer.Dest) {
		transfer.Dest = filepath.Join(transfer.Dest, transfer.Src)
	}
	destDir := filepath.Dir(transfer.Dest)
	err := os.MkdirAll(destDir, 0700)
	if err != nil {
		return fmt.Errorf("cannot create %s: %w", destDir, err)
	}
	s.file, err = os.Create(transfer.Dest)
	return err
}

func (s *ServerDest) fileAppend(args interface{}) error {
	chunk, ok := args.(TransferChunk)
	if !ok {
		return fmt.Errorf("Unexpected type to fileAppend: %v", args)
	}
	_, err := s.file.Write(chunk.Buf)
	return err
}

func (s *ServerDest) fileClose(args interface{}) error {
	err := s.file.Close()
	s.link.Send(MessageHeader{ID: ReadyID})
	return err
}

func (s *ServerDest) shutdown(args interface{}) error {
	s.end <- true
	return nil
}

func isDirectory(path string) bool {
	info, err := os.Stat(path)
	if err != nil {
		return false
	}
	return info.IsDir()
}
