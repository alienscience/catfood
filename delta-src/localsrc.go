package main

import (
	"fmt"
	"io"
	"os"
	"path"
	"time"

	"github.com/schollz/progressbar/v3"
)

const (
	bufferSize = 1024 * 1024
)

type LocalSource struct {
	srcPath  string
	progress *progressbar.ProgressBar
}

func newLocalSource(srcPath string) (*LocalSource, error) {
	size, err := fileSize(srcPath)
	if err != nil {
		return nil, fmt.Errorf("Could not calculate size of %s: %w", srcPath, err)
	}
	progress := progressbar.NewOptions64(
		size,
		progressbar.OptionSetDescription(path.Base(srcPath)),
		progressbar.OptionSetWriter(os.Stderr),
		progressbar.OptionShowBytes(true),
		progressbar.OptionSetWidth(10),
		progressbar.OptionThrottle(65*time.Millisecond),
		progressbar.OptionShowCount(),
		progressbar.OptionClearOnFinish(),
		progressbar.OptionSpinnerType(14),
		progressbar.OptionFullWidth(),
	)
	return &LocalSource{srcPath, progress}, nil
}

func (l *LocalSource) Send(remote *RemoteDest) error {
	defer closeRemoteFile(remote)
	err := l.progress.RenderBlank()
	if err != nil {
		return fmt.Errorf("cannot render progress bar: %w", err)
	}
	file, err := os.Open(l.srcPath)
	if err != nil {
		return fmt.Errorf("cannot open %s: %w", l.srcPath, err)
	}
	defer file.Close()
	buffer := make([]byte, bufferSize)
	for {
		size, err := file.Read(buffer)
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		err = remote.FileAppend(buffer[:size])
		if err != nil {
			return fmt.Errorf("cannot send chunk of %s: %w", l.srcPath, err)
		}
		l.progress.Add(size)
	}
	return nil
}

func closeRemoteFile(remote *RemoteDest) {
	remote.FileClose()
}

func fileSize(filePath string) (int64, error) {
	fi, err := os.Stat(filePath)
	if err != nil {
		return 0, err
	}
	return fi.Size(), nil
}
