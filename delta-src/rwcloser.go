package main

import "io"

type RWCloser struct {
	r io.ReadCloser
	w io.WriteCloser
}

func newRWCloser(r io.ReadCloser, w io.WriteCloser) *RWCloser {
	return &RWCloser{r, w}
}

func (r *RWCloser) Read(p []byte) (int, error) {
	return r.r.Read(p)
}

func (r *RWCloser) Write(p []byte) (int, error) {
	return r.w.Write(p)
}

func (r *RWCloser) Close() error {
	err1 := r.r.Close()
	err2 := r.w.Close()
	if err1 != nil {
		return err1
	}
	if err2 != nil {
		return err2
	}
	return nil
}
