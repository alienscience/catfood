package main

import "reflect"

type Subscriptions struct {
	link     *Channel
	handlers map[int]handler
}

type handler struct {
	argType reflect.Type
	fn      func(interface{}) error
}

func NewSubscriptions(link *Channel) *Subscriptions {
	return &Subscriptions{link, make(map[int]handler)}
}

func (s *Subscriptions) Add(id int, exampleArgs interface{}, fn func(interface{}) error) {
	var t reflect.Type
	if exampleArgs == nil {
		t = nil
	} else {
		t = reflect.TypeOf(exampleArgs)
	}
	handler := handler{t, fn}
	s.handlers[id] = handler
}

func (s *Subscriptions) Run() error {
	var msgHeader MessageHeader
	var err error
	for {
		err = s.link.Receive(&msgHeader)
		if err != nil {
			return err
		}
		handler, ok := s.handlers[msgHeader.ID]
		if !ok {
			l.Printf("Ignoring incoming Message ID %d", msgHeader.ID)
			continue
		}
		if handler.argType != nil {
			value := reflect.New(handler.argType)
			s.link.ReceiveValue(value)
			err = handler.fn(value.Elem().Interface())
		} else {
			err = handler.fn(nil)
		}
		if err != nil {
			return err
		}
	}
}
