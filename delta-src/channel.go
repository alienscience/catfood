package main

import (
	"encoding/gob"
	"io"
	"reflect"
)

type Channel struct {
	link   io.ReadWriteCloser
	output *gob.Encoder
	input  *gob.Decoder
}

func newChannel(link io.ReadWriteCloser) *Channel {
	return &Channel{
		link:   link,
		output: gob.NewEncoder(link),
		input:  gob.NewDecoder(link),
	}
}

func (c *Channel) Send(msg interface{}) error {
	return c.output.Encode(msg)
}

func (c *Channel) Receive(msg interface{}) error {
	return c.input.Decode(msg)
}

func (c *Channel) ReceiveValue(msg reflect.Value) error {
	return c.input.DecodeValue(msg)
}

func (c *Channel) Close() error {
	return c.link.Close()
}
