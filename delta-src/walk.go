package main

import (
	"io/fs"
	"path/filepath"
)

func FileTree(path string) ([]string, error) {
	contents := make([]string, 0, 16)
	err := filepath.WalkDir(path, func(p string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		// Ignore directories
		if d.IsDir() {
			return nil
		}
		contents = append(contents, p)
		return nil
	})
	return contents, err
}
