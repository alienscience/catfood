package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

const usageMessage = `
Usage:
   delta-src [options] file dest
   delta-src -s
Transfers a file.
The "dest" can be a local directory or a ssh url.
options:
`

type SubProcess struct {
	cmd    *exec.Cmd
	stdin  io.WriteCloser
	stdout io.ReadCloser
}

func main() {
	log.SetFlags(0)
	serverFlag := flag.Bool("s", false, "Serve")
	installFlag := flag.Bool("install", true, "Install delta-src on the remote")
	flag.Parse()
	if *serverFlag {
		serve()
		return
	}
	args := flag.Args()
	if len(args) < 2 {
		usage()
		os.Exit(1)
	}
	remote, err := parseRemote(args[len(args)-1])
	if err != nil {
		log.Fatal(err)
	}
	srcFiles := args[:len(args)-1]
	doInstall := isRemote(remote) && *installFlag
	var sp SubProcess
	switch {
	case doInstall:
		if err = install(*remote); err != nil {
			log.Fatal(err)
		}
		sp, err = sshClient(remote, "./delta-src")
		if err == nil {
			err = client(sp, remote.RelativePath(), srcFiles)
		}
		// TODO: cleanup
	case isRemote(remote):
		sp, err = sshClient(remote, "delta-src")
		if err != nil {
			break
		}
		err = client(sp, remote.RelativePath(), srcFiles)
	default:
		sp, err = localClient()
		if err != nil {
			break
		}
		err = client(sp, remote.RelativePath(), srcFiles)
	}
	if err != nil {
		log.Fatal(err)
	}
}

func usage() {
	fmt.Print(usageMessage)
	flag.PrintDefaults()
}

func client(sp SubProcess, destFile string, srcArgs []string) error {
	link := newRWCloser(sp.stdout, sp.stdin)
	dest, err := newRemoteDest(link)
	if err != nil {
		return fmt.Errorf("cannot create remote dest: %w", err)
	}
	for _, srcArg := range srcArgs {
		srcFiles, err := FileTree(srcArg)
		if err != nil {
			return fmt.Errorf("cannot walk %s: %w", srcArg, err)
		}
		for _, srcFile := range srcFiles {
			src, err := newLocalSource(srcFile)
			if err != nil {
				return fmt.Errorf("cannot create local source: %w", err)
			}
			if srcFile == srcArg {
				dest.TransferFile(filepath.Base(srcFile), destFile)
			} else {
				rel, err := filepath.Rel(srcArg, srcFile)
				if err != nil {
					return fmt.Errorf("%s is not relative to %s: %w", srcFile, srcArg, err)
				}
				err = dest.TransferFile(rel, destFile)
				if err != nil {
					return fmt.Errorf("cannot transfer %s: %w", rel, err)
				}
			}
			err = src.Send(dest)
			if err != nil {
				return fmt.Errorf("cannot send file: %w", err)
			}
		}
	}
	dest.End()
	return sp.cmd.Wait()
}

func localClient() (SubProcess, error) {
	return subProcess("./delta-src", "-s")
}

func isRemote(u *Urlx) bool {
	return len(u.Scheme) > 0 && len(u.Host) > 0
}

func sshClient(remote *Urlx, exe string) (SubProcess, error) {
	cmd := fmt.Sprintf("%s -s", exe)
	log.Printf("Executing ssh: ssh %s", remote.SSHString())
	return subProcess("ssh", remote.SSHString(), cmd)
}

func serve() {
	link := newRWCloser(os.Stdin, os.Stdout)
	server := newServerDest(link)
	go func() {
		err := server.Serve()
		if err != nil {
			l.Print(err)
		}
	}()
	<-server.end
}

func subProcess(command string, args ...string) (SubProcess, error) {
	cmd := exec.Command(command, args...)
	stdin, err := cmd.StdinPipe()
	if err != nil {
		return SubProcess{}, err
	}
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return SubProcess{}, err
	}
	cmd.Stderr = os.Stderr
	err = cmd.Start()
	return SubProcess{
		cmd:    cmd,
		stdin:  stdin,
		stdout: stdout,
	}, err
}

func parseRemote(dest string) (*Urlx, error) {
	remote, err := ParseURL(dest)
	if err != nil {
		return nil, err
	}
	if !isRemote(remote) {
		return remote, nil
	}
	if remote.Scheme != "ssh" {
		err = fmt.Errorf("only ssh urls supported, got %s", remote.Scheme)
		return remote, err
	}
	return remote, nil
}

func install(remote Urlx) error {
	launched, err := os.Executable()
	if err != nil {
		return fmt.Errorf("cannot get executable name: %w", err)
	}
	exe, err := filepath.EvalSymlinks(launched)
	if err != nil {
		return fmt.Errorf("cannot get executable name from symlinks: %w", err)
	}
	remote.Scheme = "scp"
	log.Printf("Installing to %s", remote.SSHString())
	cmd := exec.Command("scp", exe, remote.SSHString())
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	err = cmd.Run()
	if err != nil {
		return fmt.Errorf("could not install on remote: %w", err)
	}
	return nil
}
