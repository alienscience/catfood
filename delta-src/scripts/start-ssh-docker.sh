#!/usr/bin/env bash

set -e

docker run -d \
  --name=openssh-server \
  -e PUBLIC_KEY="$(cat $HOME/.ssh/id_ed25519.pub)" \
  -e USER_NAME=saul \
  -p 2222:2222 \
  openssh-server
