#!/bin/bash

set -e

podman run \
	--name openssh-server \
	--env PUBLIC_KEY="$(cat ~/.ssh/id_ed25519.pub)" \
	--env USER_NAME=saul \
	-dt -p 2222:2222/tcp openssh-server
