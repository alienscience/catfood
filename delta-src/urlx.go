package main

import (
	"net/url"
	"strings"
)

type Urlx struct {
	url.URL
}

func ParseURL(raw string) (*Urlx, error) {
	u, err := url.Parse(raw)
	if err != nil {
		return nil, err
	}
	return &Urlx{*u}, err
}

func (u *Urlx) RelativePath() string {
	return strings.TrimPrefix(u.Path, "/~/")
}

func (u Urlx) SSHString() string {
	u.Path = "/"
	return u.String()
}
