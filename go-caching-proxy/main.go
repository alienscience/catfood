package main

import (
	"log"
	"net/http"
	"time"
)

// Recommended by gosec.
const (
	defaultReadHeaderTimeout = 3 * time.Second
	// defaultCacheSize         = 16 * GB
	defaultCacheSize = 200 * KB
)

func main() {
	cacheControl := NewCacheControl(defaultCacheSize)
	proxy, err := NewProxy(cacheControl)
	if err != nil {
		log.Fatal(err)
	}
	//nolint: exhaustruct
	server := &http.Server{
		Addr:              ":8080",
		ReadHeaderTimeout: defaultReadHeaderTimeout,
		Handler:           proxy,
	}
	log.Fatal(server.ListenAndServe())
}
