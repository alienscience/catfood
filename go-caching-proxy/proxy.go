package main

import (
	"bufio"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
)

var ErrDoesNotSupportHijack = errors.New("response writer does not support a hijack of the client connection")
var ErrRequestBlocked = errors.New("request blocked")

type proxyError struct {
	httpStatusCode int
	err            error
}

func (e *proxyError) Error() string {
	return e.err.Error()
}

func newProxyError(code int, e error) *proxyError {
	return &proxyError{httpStatusCode: code, err: e}
}

type Proxy struct {
	cache        *Cache
	cacheControl CacheControl
}

func NewProxy(control CacheControl) (*Proxy, error) {
	cache, err := NewCache(control.MaxSizeBytes)
	if err != nil {
		return nil, fmt.Errorf("could not create proxy: %w", err)
	}
	return &Proxy{cache: cache, cacheControl: control}, nil
}

func (p *Proxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch {
	case r.Method == http.MethodConnect:
		p.proxyConnect(w, r)
	case r.Method == "GET" && r.URL.Path == "/":
		serveWelcome(w, r)
	default:
		http.NotFound(w, r)
	}
}

func serveWelcome(w http.ResponseWriter, _ *http.Request) {
	_, _ = io.WriteString(w, "Welcome to the proxy.")
}

func (p *Proxy) proxyConnect(w http.ResponseWriter, r *http.Request) {
	clientConn, err := hijackClient(w, r)
	if err != nil {
		http.Error(w, err.Error(), err.httpStatusCode)
		return
	}
	defer clientConn.Close()
	mitm, err := doMITM(clientConn, r)
	if err != nil {
		http.Error(w, err.Error(), err.httpStatusCode)
		return
	}
	defer mitm.clientTLS.Close()
	err = p.processTunnelledRequests(mitm)
	if err != nil {
		wrapped := fmt.Errorf("cannot process tunnelled request: %w", err)
		responseWriter := NewResponseWriter(mitm.clientTLS)
		http.Error(responseWriter, wrapped.Error(), err.httpStatusCode)
		return
	}
}

func hijackClient(w http.ResponseWriter, _ *http.Request) (net.Conn, *proxyError) {
	hijack, ok := w.(http.Hijacker)
	if !ok {
		return nil, newProxyError(http.StatusInternalServerError, ErrDoesNotSupportHijack)
	}
	clientConn, _, err := hijack.Hijack()
	if err != nil {
		return nil, newProxyError(
			http.StatusInternalServerError,
			fmt.Errorf("cannot hijack client connection: %w", err),
		)
	}
	_, err = io.WriteString(clientConn, "HTTP/1.0 200 OK\r\n\r\n")
	if err != nil {
		return nil, newProxyError(
			http.StatusGone,
			fmt.Errorf("cannot write OK to client: %w", err),
		)
	}
	return clientConn, nil
}

type mitm struct {
	remoteHost   string
	remotePort   string
	clientReader *bufio.Reader
	clientTLS    *tls.Conn
}

func doMITM(clientConn net.Conn, r *http.Request) (*mitm, *proxyError) {
	remoteHost, remotePort, err := net.SplitHostPort(r.URL.Host)
	if err != nil {
		return nil, newProxyError(
			http.StatusInternalServerError,
			fmt.Errorf("cannot split host_port, %s: %w", r.URL.Host, err),
		)
	}
	tlsConfig, err := MakeTLSConfig(remoteHost)
	if err != nil {
		return nil, newProxyError(
			http.StatusInternalServerError,
			fmt.Errorf("cannot create TLS config: %w", err),
		)
	}
	clientTLSServer := tls.Server(clientConn, tlsConfig)
	if err := clientTLSServer.Handshake(); err != nil {
		return nil, newProxyError(
			http.StatusBadRequest,
			fmt.Errorf("client handshake failed: %w", err),
		)
	}
	clientReader := bufio.NewReader(clientTLSServer)
	return &mitm{
		remoteHost:   remoteHost,
		remotePort:   remotePort,
		clientReader: clientReader,
		clientTLS:    clientTLSServer,
	}, nil
}

func (p *Proxy) processTunnelledRequests(mitm *mitm) *proxyError {
	for {
		tunneledRequest, err := http.ReadRequest(mitm.clientReader)
		if err != nil {
			return newProxyError(
				http.StatusBadRequest,
				fmt.Errorf("cannot read request from client: %w", err),
			)
		}
		rewriteClientRequest(tunneledRequest, mitm.remoteHost, mitm.remotePort)
		allow, err := p.cacheControl.AllowRequest(tunneledRequest)
		if err != nil {
			return newProxyError(
				http.StatusInternalServerError,
				fmt.Errorf("request to %s failed checks: %w", mitm.remoteHost, err),
			)
		}
		if !allow {
			return newProxyError(http.StatusForbidden, ErrRequestBlocked)
		}
		response, err := p.getServerResponse(tunneledRequest)
		if err != nil {
			return newProxyError(
				http.StatusBadGateway,
				fmt.Errorf("cannot get response from %s: %w", mitm.remoteHost, err),
			)
		}
		defer response.Body.Close()
		if err := doClientResponse(response, mitm.clientTLS); err != nil {
			return newProxyError(
				http.StatusInternalServerError,
				fmt.Errorf("cannot copy response to client: %w", err),
			)
		}
	}
}

func (p *Proxy) getServerResponse(request *http.Request) (*http.Response, error) {
	key := cacheKey(request)
	p.cache.Lock(key)
	defer p.cache.Unlock(key)
	checkCache, err := p.cacheControl.DirectFromCache(request)
	if err != nil {
		return nil, fmt.Errorf("cannot check cache control for request: %w", err)
	}
	if checkCache {
		if response, err := p.getCachedResponse(key); err == nil {
			return response, nil
		}
	}
	response, err := doRequest(request)
	if err != nil {
		return nil, fmt.Errorf("cannot get response from %s: %w", request.URL, err)
	}
	if response.StatusCode != http.StatusOK {
		return response, nil
	}
	canCache, err := p.cacheControl.CanCacheResponse(request, response)
	if err != nil {
		return nil, fmt.Errorf("cannot check cache control for response: %w", err)
	}
	if !canCache {
		return response, nil
	}
	if err := p.saveCachedResponse(key, response); err != nil {
		return nil, fmt.Errorf("cannot save cached response: %w", err)
	}
	return p.getCachedResponse(key)
}

func (p *Proxy) getCachedResponse(key string) (*http.Response, error) {
	//nolint: exhaustruct
	response := http.Response{
		Status:     http.StatusText(http.StatusOK),
		StatusCode: http.StatusOK,
		Proto:      "HTTP/1.1",
		ProtoMajor: 1,
		ProtoMinor: 1,
	}
	var meta cacheMeta
	if err := p.cache.GetJSON(key, metaTag, &meta); err != nil {
		return nil, fmt.Errorf("cannot get cache meta: %w", err)
	}
	response.Header = meta.Header
	body, err := p.cache.Get(key, bodyTag)
	if err != nil {
		return nil, fmt.Errorf("cannot get cache body: %w", err)
	}
	response.Body = body
	stat, err := body.Stat()
	if err != nil {
		body.Close()
		return nil, fmt.Errorf("cannot stat cache body: %w", err)
	}
	response.ContentLength = stat.Size()
	return &response, nil
}

func (p *Proxy) saveCachedResponse(key string, response *http.Response) error {
	meta := newCacheMeta(response)
	if err := p.cache.PutJSON(key, metaTag, meta); err != nil {
		return err
	}
	if err := p.cache.Put(key, bodyTag, response.Body); err != nil {
		return err
	}
	return nil
}

func rewriteClientRequest(r *http.Request, host string, port string) {
	fakeURL := "https://" + host + ":" + port + r.RequestURI
	newURL, err := url.Parse(fakeURL)
	if err != nil {
		log.Printf("Cannot parse %s: %v", fakeURL, err)
		return
	}
	r.URL = newURL
	r.RequestURI = "" // Cannot be set when calling http client
}

func doRequest(r *http.Request) (*http.Response, error) {
	var proxyClient http.Client
	serverResponse, err := proxyClient.Do(r)
	if err != nil {
		return serverResponse, fmt.Errorf("cannot connect to %s: %w", r.URL.String(), err)
	}
	return serverResponse, nil
}

func doClientResponse(response *http.Response, w io.Writer) error {
	if err := response.Write(w); err != nil {
		return fmt.Errorf("cannot write response to client: %w", err)
	}
	return nil
}
