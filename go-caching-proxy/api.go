package main

import "net/http"

const (
	_ = 1 << (10 * iota) //nolint: gomnd
	KB
	MB
	GB
	TB
)

type CacheControl struct {
	MaxSizeBytes     int64
	AllowRequest     func(request *http.Request) (bool, error)
	DirectFromCache  func(request *http.Request) (bool, error)
	CanCacheResponse func(request *http.Request, response *http.Response) (bool, error)
}

func NewCacheControl(maxSizeBytes int64) CacheControl {
	return CacheControl{
		MaxSizeBytes:     maxSizeBytes,
		AllowRequest:     alwaysTrue,
		DirectFromCache:  alwaysTrue,
		CanCacheResponse: alwaysCache,
	}
}

func alwaysTrue(_ *http.Request) (bool, error) {
	return true, nil
}

func alwaysCache(request *http.Request, response *http.Response) (bool, error) {
	return true, nil
}
