package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
)

type ResponseWriter struct {
	writer     io.Writer
	statusCode int
	header     http.Header
}

func NewResponseWriter(w io.Writer) *ResponseWriter {
	return &ResponseWriter{
		writer:     w,
		statusCode: http.StatusOK,
		header:     http.Header{},
	}
}

func (w *ResponseWriter) Header() http.Header {
	return w.header
}

func (w *ResponseWriter) Write(b []byte) (int, error) {
	written, err := fmt.Fprintf(w.writer, "HTTP/1.1 %d %s\r\n",
		w.statusCode, http.StatusText(w.statusCode))
	if err != nil {
		return written, fmt.Errorf("cannot write http status: %w", err)
	}
	var buf bytes.Buffer
	if err := w.header.Write(&buf); err != nil {
		return written, fmt.Errorf("cannot write http header to buffer: %w", err)
	}
	hdrWritten, err := io.Copy(w.writer, &buf)
	written += int(hdrWritten)
	if err != nil {
		return written, fmt.Errorf("cannot write http header: %w", err)
	}
	bodyWritten, err := w.writer.Write(b)
	written += bodyWritten
	if err != nil {
		return written, fmt.Errorf("cannot write http body: %w", err)
	}
	return written, nil
}

func (w *ResponseWriter) WriteHeader(statusCode int) {
	w.statusCode = statusCode
}
