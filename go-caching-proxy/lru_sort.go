package main

import (
	"sort"
	"time"
)

type sortableCacheEntry struct {
	key      string
	size     int64
	usedTime time.Time
}

type ByUsedTime []sortableCacheEntry

func (a ByUsedTime) Len() int {
	return len(a)
}

func (a ByUsedTime) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a ByUsedTime) Less(i, j int) bool {
	return a[i].usedTime.Before(a[j].usedTime)
}

func sortEntries(entries []sortableCacheEntry) {
	byUsedTime := ByUsedTime(entries)
	sort.Sort(byUsedTime)
}
