package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
)

var ErrNotInCache = errors.New("item not in cache")

const (
	readWriteOwner = 0600
	executeOwner   = 0100
)

const partial = ".partial"

type Cache struct {
	locks        KeyLocker
	dir          string
	maxSizeBytes int64
	evict        *LRU
}

func NewCache(maxSizeBytes int64) (*Cache, error) {
	dir := path.Join(".", "cache")
	evict, err := existingEntries(dir)
	if err != nil {
		return nil, fmt.Errorf("could not create cache: %w", err)
	}
	return &Cache{
		locks:        *NewLock(),
		dir:          dir,
		maxSizeBytes: maxSizeBytes,
		evict:        evict,
	}, nil
}

func (c *Cache) Lock(key string) {
	c.locks.Lock(key)
}

func (c *Cache) Unlock(key string) {
	c.locks.Unlock(key)
}

func (c *Cache) GetJSON(key, tag string, destination any) error {
	_, cachePath := c.itemPath(key, tag)
	file, err := os.Open(cachePath)
	if err != nil {
		return ErrNotInCache
	}
	defer file.Close()
	dec := json.NewDecoder(file)
	if err := dec.Decode(destination); err != nil {
		return fmt.Errorf("cannot decode cache meta: %w", err)
	}
	c.evict.Access(key)
	return nil
}

func (c *Cache) Get(key, tag string) (*os.File, error) {
	_, cachePath := c.itemPath(key, tag)
	file, err := os.Open(cachePath)
	if err != nil {
		return nil, ErrNotInCache
	}
	c.evict.Access(key)
	return file, nil
}

func (c *Cache) PutJSON(key, tag string, source any) error {
	subdir, cachePath := c.itemPath(key, tag)
	if err := createDir(subdir); err != nil {
		return fmt.Errorf("cannot create cache dir: %w", err)
	}
	file, err := os.OpenFile(cachePath, os.O_RDWR|os.O_CREATE, readWriteOwner)
	if err != nil {
		return fmt.Errorf("cannot create cache %s: %w", tag, err)
	}
	defer file.Close()
	enc := json.NewEncoder(file)
	if err := enc.Encode(source); err != nil {
		return fmt.Errorf("cannot serialize cache %s: %w", tag, err)
	}
	info, err := file.Stat()
	if err != nil {
		return fmt.Errorf("cannot stat %s: %w", cachePath, err)
	}
	c.evict.Add(key, info.Size())
	return nil
}

func (c *Cache) Put(key, tag string, body io.Reader) error {
	subdir, cachePath := c.itemPath(key, tag)
	pathOfPartial := cachePath + partial
	if err := createDir(subdir); err != nil {
		return fmt.Errorf("cannot create cache dir: %w", err)
	}
	file, err := os.OpenFile(pathOfPartial, os.O_RDWR|os.O_CREATE, readWriteOwner)
	if err != nil {
		return fmt.Errorf("cannot create cache partial %s: %w", tag, err)
	}
	defer file.Close()
	_, err = io.Copy(file, body)
	if err != nil {
		return fmt.Errorf("cannot copy %s to cache partial: %w", tag, err)
	}
	if err := os.Rename(pathOfPartial, cachePath); err != nil {
		return fmt.Errorf("cannot rename cache partial of %s: %w", tag, err)
	}
	// TODO: size should be of while cache entry not just this particular tag
	info, err := file.Stat()
	if err != nil {
		return fmt.Errorf("cannot stat %s: %w", cachePath, err)
	}
	c.evict.Add(key, info.Size())
	available := c.maxSizeBytes - c.evict.TotalSize()
	// TODO: remove this debug
	log.Printf("%d bytes available in cache", available)
	if available < 0 {
		err = c.reclaimSpace(-1 * available)
		if err != nil {
			return fmt.Errorf("no space after writing %s: %w", tag, err)
		}
	}
	return nil
}

func createDir(dir string) error {
	if err := os.Mkdir(dir, readWriteOwner|executeOwner); err != nil && !os.IsExist(err) {
		return fmt.Errorf("cannot create %s: %w", dir, err)
	}
	return nil
}

func (c *Cache) itemPath(key string, tag string) (string, string) {
	prefix := key[:3]
	return path.Join(c.dir, prefix), path.Join(c.dir, prefix, key) + "." + tag
}

func (c *Cache) reclaimSpace(size int64) error {
	toRemove := c.evict.ReclaimSpace(size)
	// TODO: remove this debug
	log.Printf("Reclaiming space by deleting cache keys: %v", toRemove)
	log.Printf("New total size = %d", c.evict.TotalSize())
	for _, key := range toRemove {
		// TODO: lock key being removed
		if err := c.deleteKey(key); err != nil {
			return fmt.Errorf("failed to reclaim space: %w", err)
		}
	}
	return nil
}

func (c *Cache) deleteKey(key string) error {
	// TODO: remove all files starting with key
	for _, tag := range []string{metaTag, bodyTag} {
		_, cachePath := c.itemPath(key, tag)
		log.Print("Deleting ", cachePath)
		if err := os.Remove(cachePath); err != nil {
			return fmt.Errorf("cannot remove %s: %w", cachePath, err)
		}
	}
	return nil
}

func existingEntries(dir string) (*LRU, error) {
	sortableEntries := make([]sortableCacheEntry, 0, LRUInitialSize)
	err := filepath.Walk(dir, func(pth string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		key := pathItem(pth)
		size := info.Size()
		// Access times are both system dependent and probably unavailable.
		// Fake LRU with most recently modified.
		modified := time.Since(info.ModTime())
		usedTime := time.Unix(0, 0).Add(modified)
		sortableEntries = append(sortableEntries, sortableCacheEntry{
			key:      key,
			size:     size,
			usedTime: usedTime,
		})
		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("could not populate cache: %w", err)
	}
	sortEntries(sortableEntries)
	lru := NewLRU()
	for _, entry := range sortableEntries {
		lru.Add(entry.key, entry.size)
	}
	log.Printf("Cache size = %d", lru.TotalSize())
	return lru, nil
}

// Get the key held at the given path.
func pathItem(itemPath string) string {
	f := path.Base(itemPath)
	tag := strings.LastIndex(f, ".")
	if tag > 0 {
		f = f[:tag]
	}
	return f
}
