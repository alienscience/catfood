package main

import (
	"crypto/sha256"
	"encoding/base32"
	"net/http"
	"strings"
)

const (
	metaTag = "meta"
	bodyTag = "body"
)

type cacheMeta struct {
	Header http.Header
}

func newCacheMeta(r *http.Response) *cacheMeta {
	return &cacheMeta{
		Header: r.Header,
	}
}

func cacheKey(r *http.Request) string {
	u := r.URL.String()
	hash := sha256.Sum256([]byte(u))
	b32 := base32.HexEncoding.EncodeToString(hash[:])
	return strings.TrimRight(b32, "=")
}
