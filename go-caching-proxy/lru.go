package main

import (
	"container/list"
	"sync"
)

const LRUInitialSize = 1024 * 16

type LRU struct {
	mutex     sync.Mutex
	totalSize int64
	lookup    map[string]*list.Element
	lru       *list.List
}

type entry struct {
	key  string
	size int64
}

func NewLRU() *LRU {
	return &LRU{
		mutex:     sync.Mutex{},
		totalSize: 0,
		lookup:    make(map[string]*list.Element, LRUInitialSize),
		lru:       list.New(),
	}
}

func (l *LRU) Access(key string) {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	el, ok := l.lookup[key]
	if !ok {
		return
	}
	l.lru.MoveToFront(el)
}

func (l *LRU) Add(key string, size int64) {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	if el, ok := l.lookup[key]; ok {
		item, ok := el.Value.(entry)
		if ok {
			item.size += size
			el.Value = item
		}
		l.lru.MoveToFront(el)
	} else {
		item := entry{key: key, size: size}
		el := l.lru.PushFront(item)
		l.lookup[key] = el
	}
	l.totalSize += size
}

func (l *LRU) Delete(key string) {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	el, ok := l.lookup[key]
	if !ok {
		return
	}
	item, ok := l.lru.Remove(el).(entry)
	if ok {
		l.totalSize -= item.size
	}
}

func (l *LRU) TotalSize() int64 {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	return l.totalSize
}

// Returns a slice of keys to evict.
func (l *LRU) ReclaimSpace(size int64) []string {
	l.mutex.Lock()
	defer l.mutex.Unlock()
	toRemove := size
	ret := make([]string, 0, 4) //nolint: gomnd
	for toRemove > 0 {
		back := l.lru.Back()
		if back == nil {
			return ret
		}
		item, ok := back.Value.(entry)
		if !ok {
			continue
		}
		l.remove(item.key)
		ret = append(ret, item.key)
		toRemove -= item.size
	}
	return ret
}

func (l *LRU) remove(key string) {
	el, ok := l.lookup[key]
	if !ok {
		return
	}
	delete(l.lookup, key)
	item, ok := l.lru.Remove(el).(entry)
	if ok {
		l.totalSize -= item.size
	}
}
