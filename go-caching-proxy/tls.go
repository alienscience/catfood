package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	cryptorand "crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"fmt"
	"log"
	"math/big"
	mathrand "math/rand"
	"time"
)

const certificateExpiresIn = time.Hour * 24 * 7

func MakeTLSConfig(host string) (*tls.Config, error) {
	tlsConfig := &tls.Config{MinVersion: tls.VersionTLS12} //nolint: exhaustruct
	// TODO: use a CA
	cert, err := generateCert(host, nil)
	if err != nil {
		return nil, fmt.Errorf("cannot generate cert for %s: %w", host, err)
	}
	tlsConfig.Certificates = append(tlsConfig.Certificates, *cert)
	return tlsConfig, nil
}

func generateCert(host string, _ *x509.Certificate) (*tls.Certificate, error) {
	// TODO: handle a non nil ca
	privateKey, err := ecdsa.GenerateKey(elliptic.P256(), cryptorand.Reader)
	if err != nil {
		log.Fatal(err)
	}
	serialNumber := big.NewInt(mathrand.Int63())
	//nolint: exhaustruct
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Planetary Bombardment"},
			CommonName:   host,
		},
		DNSNames:  []string{host},
		NotBefore: time.Now(),
		NotAfter:  time.Now().Add(certificateExpiresIn),

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}
	der, err := x509.CreateCertificate(cryptorand.Reader,
		&template, &template,
		&privateKey.PublicKey, privateKey)
	if err != nil {
		return nil, fmt.Errorf("cannot create self-signed certificate: %w", err)
	}
	//nolint: exhaustruct
	cert := &tls.Certificate{
		Certificate: [][]byte{der},
		PrivateKey:  privateKey,
	}
	return cert, nil
}
