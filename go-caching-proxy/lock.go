package main

import "sync"

// KeyLocker supports locking by key.
type KeyLocker struct {
	globalMutex sync.Mutex
	locks       map[string]*singleLock
}

type singleLock struct {
	count int
	lock  sync.Mutex
}

func NewLock() *KeyLocker {
	return &KeyLocker{
		globalMutex: sync.Mutex{},
		locks:       make(map[string]*singleLock),
	}
}

func (l *KeyLocker) Lock(key string) {
	single := l.getInc(key)
	single.lock.Lock()
}

func (l *KeyLocker) Unlock(key string) {
	single := l.getDec(key)
	if single == nil {
		return
	}
	single.lock.Unlock()
}

func (l *KeyLocker) getInc(key string) *singleLock {
	l.globalMutex.Lock()
	defer l.globalMutex.Unlock()
	single, ok := l.locks[key]
	if !ok {
		single = &singleLock{
			count: 0,
			lock:  sync.Mutex{},
		}
		l.locks[key] = single
	}
	single.count++
	return single
}

func (l *KeyLocker) getDec(key string) *singleLock {
	l.globalMutex.Lock()
	defer l.globalMutex.Unlock()
	single, ok := l.locks[key]
	if !ok {
		return nil
	}
	single.count--
	if single.count == 0 {
		delete(l.locks, key)
	}
	return single
}
