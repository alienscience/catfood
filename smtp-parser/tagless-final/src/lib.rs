mod fun;
pub mod generator;
pub mod grammar;
pub mod parser;

#[cfg(test)]
mod tests {
    use crate::{
        generator::{generate, Generator},
        grammar::Grammar,
        parser::{parse, Parser},
    };
    use bstr::{BString, ByteSlice};

    #[derive(PartialEq, Debug, Clone)]
    enum Token {
        Who(BString),
    }

    fn seq_of_lit<G>() -> G::Repr
    where
        G: Grammar<Tok = Token>,
    {
        let who = G::token(G::lit("world"), |w| Token::Who(w.into()));
        G::seq([G::lit("Hello"), G::lit(" "), who])
    }

    #[test]
    fn parse_sequence_of_literals() {
        let parser = seq_of_lit::<Parser<Token>>();
        let res = parse(&parser, "Hello world");
        assert!(res
            .and_then(|r| r.tokens().next())
            .filter(|t| *t == Token::Who("world".into()))
            .is_some());
        let res = parse(&parser, "Hello fish");
        assert!(res.is_none());
    }

    #[test]
    fn generate_sequence_of_literals() {
        let generator = seq_of_lit::<Generator<Token>>();
        let res = generate(&generator);
        assert_eq!(res.output(), b"Hello world");
        assert_eq!(res.tokens().next(), Some(Token::Who("world".into())));
    }

    fn seq_of_alt<G>() -> G::Repr
    where
        G: Grammar<Tok = Token>,
    {
        let who = G::one_of([G::lit("fish"), G::lit("world")]);
        let who = G::token(who, |w| Token::Who(w.into()));
        G::seq([G::lit("Hello"), G::lit(" "), who])
    }

    #[test]
    fn parse_sequence_of_alternates() {
        let parser = seq_of_alt::<Parser<Token>>();
        let res = parse(&parser, "Hello world");
        assert!(res
            .and_then(|r| r.tokens().next())
            .filter(|t| *t == Token::Who("world".into()))
            .is_some());
        let res = parse(&parser, "Hello fish");
        assert!(res
            .and_then(|r| r.tokens().next())
            .filter(|t| *t == Token::Who("fish".into()))
            .is_some());
        let res = parse(&parser, "Hello television");
        assert!(res.is_none());
    }

    #[test]
    fn generate_sequence_of_alternates() {
        let generator = seq_of_alt::<Generator<Token>>();
        let (mut fish, mut world) = (0, 0);
        for _ in 0..1000 {
            let res = generate(&generator);
            if res.output() == b"Hello world"
                && res.tokens().next() == Some(Token::Who("world".into()))
            {
                world += 1;
            } else if res.output() == b"Hello fish"
                && res.tokens().next() == Some(Token::Who("fish".into()))
            {
                fish += 1;
            } else {
                unreachable!("Valid patterns did not match")
            }
            if world >= 1 && fish >= 1 {
                return;
            }
        }
        unreachable!("Only one pattern seen")
    }

    fn repeats<G>() -> G::Repr
    where
        G: Grammar<Tok = Token>,
    {
        let range = G::repeat(G::lit("yadda"), 2, 4);
        G::seq([G::lit("repeats"), G::lit(" "), range])
    }

    #[test]
    fn parse_repeats() {
        let parser = repeats::<Parser<Token>>();
        let res = parse(&parser, "repeats yaddayadda");
        assert!(res.is_some());
        let res = parse(&parser, "repeats yadda");
        assert!(res.is_none());
        let res = parse(&parser, "repeats ");
        assert!(res.is_none());
        let res = parse(&parser, "repeats yaddayaddayaddayadda");
        assert!(res.is_some());
        let res = parse(&parser, "repeats yaddayaddayaddayaddayadda");
        assert!(matches!(res, Some(st) if st.remaining() == b"yadda"));
    }

    #[test]
    fn generate_repeats() {
        let generator = repeats::<Generator<Token>>();
        let res = generate(&generator);
        assert!(res.output().starts_with(b"repeats yaddayadda"));
        assert!(res.output().ends_with(b"yaddayadda"));
        assert!(res.output().len() < 29);
    }

    fn recursive<G>() -> G::Repr
    where
        G: Grammar<Tok = Token>,
        G::Repr: Clone,
    {
        let word = G::lit("expr");
        let (expr_ref, expr) = G::forward();
        let bracketed_expr = G::seq([G::lit("{"), expr, G::lit("}")]);
        expr_ref(G::one_of([word, bracketed_expr.clone()]));
        bracketed_expr
    }

    #[test]
    fn parse_recursive() {
        let parser = recursive::<Parser<Token>>();
        let res = parse(&parser, "{expr}");
        assert!(res.is_some());
        let res = parse(&parser, "{{{{{expr}}}}}");
        assert!(res.is_some());
    }

    #[test]
    fn generate_recursive() {
        let generator = recursive::<Generator<Token>>();
        let res = generate(&generator);
        assert!(res.output().starts_with(b"{"));
        assert!(res.output().contains_str("expr"));
        assert!(res.output().ends_with(b"}"));
        let open_curlies = res.output().find_iter("{").count();
        let close_curlies = res.output().find_iter("}").count();
        assert_eq!(open_curlies, close_curlies);
    }
}
