use std::rc::Rc;

pub type Fun<A, B> = Rc<dyn Fn(A) -> B>;

pub fn make_fun<A, B, F>(f: F) -> Fun<A, B>
where
    F: Fn(A) -> B,
    for<'a> F: 'a,
{
    Rc::new(f)
}
