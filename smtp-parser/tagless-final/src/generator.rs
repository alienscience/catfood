use crate::{
    fun::{make_fun, Fun},
    grammar::{FunForward, Grammar},
};

use rand::{rngs::ThreadRng, seq::SliceRandom, thread_rng, Rng};
use std::{cell::RefCell, marker::PhantomData, rc::Rc};

type GeneratorFun<T> = Fun<GeneratorState<T>, GeneratorState<T>>;

#[derive(Clone)]
pub struct GeneratorState<Tok> {
    random: ThreadRng,
    output: Vec<u8>,
    tokens: Vec<Tok>,
}

impl<Tok> GeneratorState<Tok>
where
    Tok: Clone,
{
    pub fn output(&self) -> &[u8] {
        &self.output
    }

    pub fn tokens(&self) -> impl Iterator<Item = Tok> + '_ {
        self.tokens.iter().cloned()
    }
}

#[derive(Clone, Copy)]
pub struct Generator<Tok> {
    phantom: PhantomData<Tok>,
}

pub fn generate<Tok>(gen: &GeneratorFun<Tok>) -> GeneratorState<Tok>
where
    Tok: Clone,
{
    let state: GeneratorState<Tok> = GeneratorState {
        random: thread_rng(),
        output: Vec::new(),
        tokens: Vec::new(),
    };
    gen(state)
}

impl<Tok> Grammar for Generator<Tok>
where
    Tok: Clone + 'static,
{
    type Repr = GeneratorFun<Tok>;
    type Tok = Tok;

    fn lit<S>(s: S) -> Self::Repr
    where
        S: Into<Vec<u8>>,
    {
        let s: Vec<u8> = s.into();
        make_fun(move |mut st: GeneratorState<Tok>| {
            st.output.extend_from_slice(&s);
            st
        })
    }

    fn seq<V>(v: V) -> Self::Repr
    where
        V: Into<Vec<Self::Repr>>,
    {
        let v: Vec<Self::Repr> = v.into();
        make_fun(move |mut st: GeneratorState<Tok>| {
            for gen in &v {
                st = gen(st);
            }
            st
        })
    }

    fn one_of<V>(v: V) -> Self::Repr
    where
        V: Into<Vec<Self::Repr>>,
    {
        let v: Vec<Self::Repr> = v.into();
        make_fun(move |mut st: GeneratorState<Tok>| {
            v.choose(&mut st.random)
                .map(|gen| gen(st.clone()))
                .unwrap_or(st)
        })
    }

    fn token<F>(generator: Self::Repr, make_token: F) -> Self::Repr
    where
        F: Fn(&[u8]) -> Self::Tok + 'static,
    {
        make_fun(move |st: GeneratorState<Tok>| {
            let prev_len = st.output.len();
            let mut st_new = generator(st);
            let token = make_token(&st_new.output[prev_len..]);
            st_new.tokens.push(token);
            st_new
        })
    }

    fn repeat(generator: Self::Repr, min: usize, max: usize) -> Self::Repr {
        make_fun(move |mut st: GeneratorState<Tok>| {
            let repeats = st.random.gen_range(min..=max);
            for _ in 0..repeats {
                st = generator(st);
            }
            st
        })
    }

    fn forward() -> (FunForward<Self::Repr>, Self::Repr) {
        let set_call = Rc::new(RefCell::new(Self::empty()));
        let get_call = set_call.clone();
        let caller = make_fun(move |st: GeneratorState<Tok>| (get_call.borrow())(st));
        let setter = Box::new(move |f: Self::Repr| {
            *set_call.borrow_mut() = f;
        });
        (setter, caller)
    }
}
