use crate::{
    fun::{make_fun, Fun},
    grammar::{FunForward, Grammar},
};
use std::{cell::RefCell, marker::PhantomData, rc::Rc};

type ParserFun<T> = Fun<ParserState<T>, Option<ParserState<T>>>;

#[derive(Clone)]
pub struct ParserState<Tok>
where
    Tok: Clone,
{
    todo: Rc<Vec<u8>>,
    tokens: Vec<Tok>,
    idx: usize,
}

impl<Tok> ParserState<Tok>
where
    Tok: Clone,
{
    pub fn remaining(&self) -> &[u8] {
        &self.todo[self.idx..]
    }

    pub fn tokens(&self) -> impl Iterator<Item = Tok> + '_ {
        self.tokens.iter().cloned()
    }
}

#[derive(Clone, Copy)]
pub struct Parser<Tok> {
    phantom: PhantomData<Tok>,
}

pub fn parse<S, Tok>(p: &ParserFun<Tok>, s: S) -> Option<ParserState<Tok>>
where
    S: Into<Vec<u8>>,
    Tok: Clone,
{
    let state: ParserState<Tok> = ParserState {
        todo: Rc::new(s.into()),
        tokens: Vec::new(),
        idx: 0,
    };
    p(state)
}

impl<Tok> Grammar for Parser<Tok>
where
    Tok: Clone + 'static,
{
    type Repr = ParserFun<Tok>;
    type Tok = Tok;

    fn lit<S>(s: S) -> Self::Repr
    where
        S: Into<Vec<u8>>,
    {
        let s: Vec<u8> = s.into();
        make_fun(move |st: ParserState<Tok>| {
            st.todo[st.idx..]
                .strip_prefix(s.as_slice())
                .map(|_pre| ParserState {
                    todo: st.todo.clone(),
                    tokens: st.tokens,
                    idx: st.idx + s.len(),
                })
        })
    }

    fn seq<V>(v: V) -> Self::Repr
    where
        V: Into<Vec<Self::Repr>>,
    {
        let v: Vec<Self::Repr> = v.into();
        make_fun(move |st: ParserState<Tok>| {
            let mut st_new = st;
            for p in &v {
                if let Some(s) = p(st_new) {
                    st_new = s;
                } else {
                    return None;
                }
            }
            Some(st_new)
        })
    }

    fn one_of<V>(v: V) -> Self::Repr
    where
        V: Into<Vec<Self::Repr>>,
    {
        let v: Vec<Self::Repr> = v.into();
        make_fun(move |st: ParserState<Tok>| {
            for p in &v {
                if let Some(s) = p(st.clone()) {
                    return Some(s);
                }
            }
            None
        })
    }

    fn token<F>(parser: Self::Repr, make_token: F) -> Self::Repr
    where
        F: Fn(&[u8]) -> Self::Tok + 'static,
    {
        make_fun(move |st: ParserState<Tok>| {
            let start = st.idx;
            if let Some(mut s) = parser(st) {
                let tok = make_token(&s.todo[start..s.idx]);
                s.tokens.push(tok);
                Some(s)
            } else {
                None
            }
        })
    }

    fn repeat(parser: Self::Repr, min: usize, max: usize) -> Self::Repr {
        make_fun(move |mut st: ParserState<Tok>| {
            let mut num_matches = 0;
            while num_matches < max {
                if let Some(s) = parser(st.clone()) {
                    st = s;
                    num_matches += 1;
                } else {
                    break;
                }
            }
            if num_matches < min {
                None
            } else {
                Some(st)
            }
        })
    }

    fn forward() -> (FunForward<Self::Repr>, Self::Repr) {
        let set_call = Rc::new(RefCell::new(Self::empty()));
        let get_call = set_call.clone();
        let caller = make_fun(move |st: ParserState<Tok>| (get_call.borrow())(st));
        let setter = Box::new(move |f: Self::Repr| {
            *set_call.borrow_mut() = f;
        });
        (setter, caller)
    }
}
