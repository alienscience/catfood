pub type FunForward<T> = Box<dyn Fn(T)>;

pub trait Grammar {
    type Repr;
    type Tok;

    fn lit<S>(s: S) -> Self::Repr
    where
        S: Into<Vec<u8>>;
    fn seq<V>(v: V) -> Self::Repr
    where
        V: Into<Vec<Self::Repr>>;
    fn one_of<V>(v: V) -> Self::Repr
    where
        V: Into<Vec<Self::Repr>>;
    fn token<F>(g: Self::Repr, f: F) -> Self::Repr
    where
        F: Fn(&[u8]) -> Self::Tok + 'static;
    fn repeat(g: Self::Repr, min: usize, max: usize) -> Self::Repr;

    fn empty() -> Self::Repr {
        Self::lit("")
    }

    fn forward() -> (FunForward<Self::Repr>, Self::Repr);
}
