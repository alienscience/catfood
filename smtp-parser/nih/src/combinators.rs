use crate::parsers::*;

struct SeqParser<T: Clone> {
    parsers: Vec<Parser<T>>,
}

impl<T: Token> P<T> for SeqParser<T> {
    fn parse<'a>(&self, s: State<'a, T>) -> Option<State<'a, T>> {
        let mut s = s;
        for p in &self.parsers {
            match p.parse(s) {
                Some(st) => s = st,
                None => return None,
            }
        }
        Some(s)
    }
}

pub fn seq<const L: usize, T: Token>(parsers: [Parser<T>; L]) -> Parser<T> {
    Box::new(SeqParser {
        parsers: parsers.into(),
    })
}

struct OneOfParser<T: Clone> {
    parsers: Vec<Parser<T>>,
}

impl<T: Token> P<T> for OneOfParser<T> {
    fn parse<'a>(&self, s: State<'a, T>) -> Option<State<'a, T>> {
        for p in &self.parsers {
            let res = p.parse(s.clone());
            if res.is_some() {
                return res;
            }
        }
        None
    }
}

pub fn one_of<const L: usize, T: Token>(parsers: [Parser<T>; L]) -> Parser<T> {
    Box::new(OneOfParser {
        parsers: parsers.into(),
    })
}

struct Emit<T: Clone> {
    parser: Parser<T>,
    make_token: fn(&[u8]) -> T,
}

impl<T: Token> P<T> for Emit<T> {
    fn parse<'a>(&self, s: State<'a, T>) -> Option<State<'a, T>> {
        let mut ret = self.parser.parse(s.clone());
        if let Some(r) = &mut ret {
            let captured = s.todo.len() - r.todo.len();
            let token = (self.make_token)(&s.todo[..captured]);
            let mut extra_tokens = r.tokens.split_off(s.tokens.len());
            r.tokens.push(token);
            r.tokens.append(&mut extra_tokens);
        }
        ret
    }
}

pub fn emit<T: Token>(parser: Parser<T>, make_token: fn(&[u8]) -> T) -> Parser<T> {
    Box::new(Emit { parser, make_token })
}

struct Repeat<T: Clone> {
    parser: Parser<T>,
    min_repeat: u32,
    max_repeat: u32,
}

impl<T: Token> P<T> for Repeat<T> {
    fn parse<'a>(&self, s: State<'a, T>) -> Option<State<'a, T>> {
        let mut s = s;
        let mut i = 0;
        loop {
            if i >= self.max_repeat {
                return Some(s);
            }
            match self.parser.parse(s.clone()) {
                Some(st) => {
                    s = st;
                    i += 1;
                }
                None => {
                    if i < self.min_repeat {
                        return None;
                    } else {
                        return Some(s);
                    }
                }
            }
        }
    }
}

pub fn repeat<T: Token>(parser: Parser<T>, min_repeat: u32, max_repeat: u32) -> Parser<T> {
    Box::new(Repeat {
        parser,
        min_repeat,
        max_repeat,
    })
}

// Assuming second_parser is a subset of first_parser, run first_parser until second_parser fails.
// Then backtrack, until the second_parser is successful.
struct RepeatWhile<T: Clone> {
    min_repeat: u32,
    max_repeat: u32,
    first_parser: Parser<T>,
    second_parser: Parser<T>,
}

impl<T: Token> P<T> for RepeatWhile<T> {
    fn parse<'a>(&self, s: State<'a, T>) -> Option<State<'a, T>> {
        let original_state = s.clone();
        let mut backtrack = Vec::new();
        let mut i = 0;
        let mut ret = s;
        while i < self.max_repeat {
            match self.first_parser.parse(ret) {
                Some(s) => {
                    backtrack.push(s.clone());
                    ret = s;
                    i += 1;
                }
                None if i < self.min_repeat => return None,
                None => break,
            }
        }
        for b in backtrack.into_iter() {
            let s = self.second_parser.parse(b);
            if s.is_some() {
                return s;
            }
            i -= 1;
            if i < self.min_repeat {
                return None;
            }
        }
        Some(original_state)
    }
}
