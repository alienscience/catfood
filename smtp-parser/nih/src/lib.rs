mod combinators;
mod parsers;

pub use crate::combinators::*;
pub use crate::parsers::*;

#[cfg(test)]
mod test {
    use super::*;

    fn assert_token(s: Option<State<String>>, is_value: &str) {
        let is_value = is_value.to_string();
        if let Some(res) = s {
            let tok = res.tokens.get(0).cloned().unwrap();
            assert!(tok == is_value.to_string());
        } else {
            assert!(false, "parsing failed");
        }
    }

    #[test]
    fn tags() {
        let parser: Parser<()> = seq([tag_str("Hello"), tag_str(" "), tag_str("world")]);
        let res = parser.parse_bytes(b"Hello world");
        assert!(res.is_some());
    }

    #[derive(Clone, PartialEq, Debug)]
    enum TestToken {
        Greeting(Vec<u8>),
        Who(Vec<u8>),
        Sentence(Vec<u8>),
    }

    #[test]
    fn single_emit() {
        let parser = seq([
            tag_str("Hello"),
            tag_str(" "),
            emit(tag_str("world"), |s| TestToken::Who(s.to_vec())),
            tag_str("."),
        ]);
        let res = parser.parse_bytes(b"Hello world.");
        assert!(res.is_some());
        let tok = res.and_then(|r| r.tokens.get(0).cloned()).unwrap();
        assert!(tok == TestToken::Who(b"world".to_vec()));
    }

    #[test]
    fn hierachical_emit() {
        let parser = seq([
            emit(tag_str("Hello"), |s| TestToken::Greeting(s.to_vec())),
            tag_str(" "),
            emit(tag_str("world"), |s| TestToken::Who(s.to_vec())),
            tag_str("."),
        ]);
        let parser = emit(parser, |s| TestToken::Sentence(s.to_vec()));
        let res = parser.parse_bytes(b"Hello world.");
        assert!(res.is_some());
        let tok: Vec<_> = res.unwrap().tokens().collect();
        assert!(
            tok == [
                TestToken::Sentence(b"Hello world.".to_vec()),
                TestToken::Greeting(b"Hello".to_vec()),
                TestToken::Who(b"world".to_vec())
            ]
            .to_vec()
        );
    }

    #[test]
    fn alphanum_repeat() {
        let parser = repeat(is_alphanumeric(), 1, 8);
        let parser = emit(parser, |s| String::from_utf8_lossy(s).into_owned());
        let res = parser.parse_bytes(b"ABC123456789abcdef");
        assert_token(res, "ABC12345");
    }

    #[test]
    fn alphanum_too_short() {
        let parser = repeat(is_alphanumeric(), 7, 8);
        let parser = emit(parser, |s| String::from_utf8_lossy(s).into_owned());
        let res = parser.parse_bytes(b"ABC123");
        assert!(res.is_none());
    }

    #[test]
    fn one_of_present() {
        let parser = one_of([tag_str("one"), tag_str("two"), tag_str("three")]);
        let parser = emit(parser, |s| String::from_utf8_lossy(s).into_owned());
        let res = parser.parse_bytes(b"twofish");
        assert_token(res, "two");
    }

    #[test]
    fn one_of_not_present() {
        let parser = one_of([tag_str("one"), tag_str("two"), tag_str("three")]);
        let parser = emit(parser, |s| String::from_utf8_lossy(s).into_owned());
        let res = parser.parse_bytes(b"fishfish");
        assert!(res.is_none());
    }
}
