use bstr::decode_utf8;
use pvec::PVec;
use std::fmt::Debug;

pub trait Token: Clone + Debug + 'static {}
impl<T: Clone + Debug + 'static> Token for T {}

#[derive(Clone)]
pub struct State<'a, T: Token> {
    pub(crate) todo: &'a [u8],
    pub(crate) tokens: PVec<T>,
}

impl<'a, T: Token> State<'a, T> {
    pub fn new(buf: &'a [u8]) -> Self {
        Self {
            todo: buf,
            tokens: PVec::new(),
        }
    }

    pub fn tokens(self) -> impl Iterator<Item = T> {
        self.tokens.into_iter()
    }
}

pub trait P<T: Token> {
    fn parse<'a>(&self, s: State<'a, T>) -> Option<State<'a, T>>;

    fn parse_bytes<'a>(&self, buf: &'a [u8]) -> Option<State<'a, T>> {
        let state = State::new(buf);
        self.parse(state)
    }
}

pub type Parser<T> = Box<dyn P<T>>;

struct Tag {
    tag: Vec<u8>,
}

impl<T: Token> P<T> for Tag {
    fn parse<'a>(&self, s: State<'a, T>) -> Option<State<'a, T>> {
        if s.todo.starts_with(&self.tag) {
            return Some(State {
                todo: &s.todo[self.tag.len()..],
                tokens: s.tokens,
            });
        }
        None
    }
}

pub fn tag<T: Token>(t: &[u8]) -> Parser<T> {
    Box::new(Tag { tag: t.to_vec() })
}

pub fn tag_str<T: Token>(t: &str) -> Parser<T> {
    let tag_bytes = t.as_bytes().to_vec();
    Box::new(Tag { tag: tag_bytes })
}

struct IsAlphaNumeric {}

pub fn is_alphanumeric<T: Token>() -> Parser<T> {
    Box::new(IsAlphaNumeric {})
}

impl<T: Token> P<T> for IsAlphaNumeric {
    fn parse<'a>(&self, s: State<'a, T>) -> Option<State<'a, T>> {
        let (ch, _) = decode_utf8(s.todo);
        if ch.map(char::is_alphanumeric).unwrap_or(false) {
            return Some(State {
                todo: &s.todo[1..],
                tokens: s.tokens,
            });
        }
        None
    }
}
