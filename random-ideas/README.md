
# Random Ideas

## Crateness

An unopinionated search for Rust crates.

- Use [Melisearch](https://blog.meilisearch.com/search-rust-crates-meili/) to search crates.
- Show crate information.
    - Enhancement: Maybe show [scc](https://github.com/boyter/scc) metrics.
    - Enhancement: Crate of the week.
    - Enhancement: Check dependencies.

## Httpsaurus

A threaded HTTP server.

- Acceptor thread writes to mpmc channel.
- Epoll thread writes to same mpmc channel.
- Thread per request.
- Blocking IO.
- Request can continue after response.
- At end of handler it is possible to keep connection alive.
- Live connections get passed to Epoll thread.
    - Maybe use a datastructure where epoll marks fds as ready and worker threads add fds for an epoll.
    - Or look at implementing an epoll where extra fds can be added?

## Magic Cloud

- Users create infrastructure with terraform.
    - `git push staging`
- Users deploy apps with terraform?
    - Or toml?
    - `git push staging`
- Uses capability based security.
- Maybe runs on Firecracker/Wireguard.
    - E.g Firecracker for Docker Images: https://github.com/weaveworks/ignite
    - Maybe use tailscale for networking at first.

## Magic S3

- A library for writing S3 compatible servers.
- Embeddable in other apps.
    - Sans IO
    - Actix
    - Axum

## Single Speed Auth

- A headless authentication server.
- Supports user/pass + opaque tokens + introspection
- Support 2 factor
- Example frontend
- Embeddable in other apps if needed.
    - Sans IO
- Swappable email/notification methods.
- Swappable storage.
    - Dynamo DB
    - SQLite + Litestream
    - Postgres

## Event Sourcing Store

- Like that [old Java library](https://prevayler.org/)?
- Events written to disk.
    - Events also written elsewhere?
- Snapshots to disk.
    - Snapshots also written elsewhere?

```rust
    // Must be serde serializable
    struct MyData {}
    // Must be serde serializable
    enum MyEvent {}    
    let prevayler = Prevaylor::builder(my_data, my_execute)
        .with_wal(ev2file("/whatever"))
        .with_stream(redis) // Does not block, can also use redis as a wal
        .with_file_snaphots() // Default, uses wal path
        .with_snapshot(s3Obj)
        .with_serializer(JsonSerializer::new())
        .build();
    prevayler.transaction(&event);
```