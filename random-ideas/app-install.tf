
resource "magic_app" "example" {
  template = "rust"

  ingress = {
    port = 8000
    hosts = ["myapp.example.com"]
    path_prefix = "/test/"
  }

  // A load balancer is create automatically
  replicas = 1
  
  resources = {
    memory = "100Mi"
    cpu = "0.1"
  }

  db_connections = [data.magic_db.postgres, data.magic_db.redis]
}

// A DB is not created here because it is considered to be "infrastructure"
data "magic_db" "postgres" {
  name = "my_app_db"
}

data "magic_db" "redis" {
  name = "my_app_redis"
}

