package main

import (
	"encoding/gob"
	"fmt"
	"log"
	"os"
	"path"
)


func tabletPath(id string) string {
	return path.Join("db", id)
}

func readTablet(id string) (tablet *Tablet, err error) {
	path := tabletPath(id)
	f, e := os.Open(path)
	if e != nil {
		err = fmt.Errorf("could not open tablet %s: %w", id, err)
		return
	}
	defer func() {
		e := f.Close()
		if e != nil && err != nil {
			err = fmt.Errorf("could not close tablet %s: %w", id, err)
		}
	}()
	dec := gob.NewDecoder(f)
	err = dec.Decode(&tablet)
	return
}

func writeTablet(tablet *Tablet) (err error) {
	id := tablet.ID
	path := tabletPath(id)
	f, e := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if e != nil {
		err = fmt.Errorf("could not open tablet for writing %s: %w", id, err)
		return
	}
	defer func() {
		e := f.Close()
		if e != nil && err != nil {
			err = fmt.Errorf("could not close tablet %s: %w", id, err)
		}
	}()
	enc := gob.NewEncoder(f)
	err = enc.Encode(tablet)
	return
}

func main() {
	id := "some-id"
	tablet := &Tablet{ID: id}
	writeTablet(tablet)
	uploadTablet(tablet)
	tablet, err := readTablet(id)
	if err != nil {
		log.Fatal(err)
	}
	log.Print("Got  tablet from file with id ", tablet.ID)
	tablet, err = downloadTablet(id)
	if err != nil {
		log.Fatal(err)
	}
	log.Print("Got  tablet from S3 with id ", tablet.ID)
}
