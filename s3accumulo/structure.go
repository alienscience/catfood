package main

import (
	"math"
	"strings"
)

const (
	notFound = -1
)

type Tablet struct {
	ID     string
	Blocks []Block
}

type Block struct {
	Max       string
	KeyValues []KeyValue
}

type KeyValue struct {
	Key   Key
	Value []byte
}

type Key struct {
	RowID     string
	Column    Column
	Timestamp int64
}

type Column struct {
	Family     string
	Qualifier  string
	Visibility string
}

type Position struct {
	Row int64
}

type Index struct {
	BlockIdx int
	RowIdx   int
}

// Returns nil if no block can be found but will return
// an Index with RowIdx == notFound if a block exists but not the rowId
func (t *Tablet) FindRow(RowID string) *Index {
	block, blockIdx := t.findBlock(RowID)
	if block == nil {
		return nil
	}
	rowIdx := block.findRow(RowID)
	return &Index{BlockIdx: blockIdx, RowIdx: rowIdx}
}

func (t *Tablet) InsertKeyValue(kv KeyValue) {
	rowID := kv.Key.RowID
	idx := t.FindRow(rowID)
	if idx == nil {
		first := &t.Blocks[0]
		if strings.Compare(rowID, first.Min) < 0 {
			first.prependKeyValue(kv)
		} else {
			last := t.Blocks[len(t.Blocks)-1]
			last.appendKeyValue(kv)
		}
		return
	}
	t.Blocks[idx.BlockIdx].insertKeyValueWithIndex(kv, idx.RowIdx)
}

func (t *Tablet) InsertKeyValueWithIndex(kv KeyValue, idx Index) {
	t.Blocks[idx.BlockIdx].insertKeyValueWithIndex(kv, idx.RowIdx)
}

func (t *Tablet) findBlock(RowID string) (*Block, int) {
	start := 0
	end := len(t.Blocks) - 1
	for start <= end {
		middle := int(math.Floor((float64(end) - float64(start)) / 2.0))
		block := t.Blocks[middle]
		if middle > 0 && strings.Compare(RowID, t.Blocks[middle-1].Max) > 0 {
			start = middle + 1
		} else if strings.Compare(RowID, block.Max) < 0 {
			end = middle - 1
		} else {
			return &block, middle
		}
	}
	return nil, notFound
}

// Find the index in the block that is the start of the given row
func (b *Block) findRow(RowID string) int {
	start := 0
	end := len(b.KeyValues) - 1
	for start <= end {
		middle := int(math.Floor((float64(end) - float64(start)) / 2.0))
		row := b.KeyValues[middle]
		cmp := strings.Compare(RowID, row.Key.RowID)
		if cmp < 0 {
			start = middle + 1
		} else if cmp > 0 {
			end = middle - 1
		} else if b.KeyValues[middle-1].Key.RowID != RowID {
			return middle
		} else {
			end = middle - 1
		}
	}
	return notFound
}

func (b *Block) prependKeyValue(kv KeyValue) {
	rowID := kv.Key.RowID
	b.KeyValues = append([]KeyValue{kv}, b.KeyValues...)
	b.Min = rowID
}

func (b *Block) appendKeyValue(kv KeyValue) {
	rowID := kv.Key.RowID
	b.KeyValues = append(b.KeyValues, kv)
	b.Max = rowID
}

func (b *Block) insertKeyValueWithIndex(kv KeyValue, idx int) {
	if idx == notFound {
		b.insertKeyValue(kv)
		return
	}
	b.KeyValues[idx] = kv
	// TODO: finish
}
