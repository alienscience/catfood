package main

import (
	"bytes"
	"encoding/gob"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

const (
	Bucket = "s3db"
)

var S3Config = &aws.Config{
	Credentials:      credentials.NewStaticCredentials("minioadmin", "minioadmin", ""),
	Endpoint:         aws.String("http://localhost:9000"),
	Region:           aws.String("us-east-1"),
	DisableSSL:       aws.Bool(true),
	S3ForcePathStyle: aws.Bool(true),
}

func uploadTablet(tablet *Tablet) error {
	id := tablet.ID
	key := tabletPath(id)
	sess := session.Must(session.NewSession(S3Config))
	uploader := s3manager.NewUploader(sess)
	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	err := enc.Encode(tablet)
	if err != nil {
		return fmt.Errorf("failed to serialize tablet %s: %w", id, err)
	}
	// Upload the buffer to S3.
	_, err = uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(Bucket),
		Key:    aws.String(key),
		Body:   buf,
	})
	if err != nil {
		return fmt.Errorf("failed to upload tablet %s: %w", id, err)
	}
	return nil
}

func downloadTablet(id string) (*Tablet, error) {
	key := tabletPath(id)
	sess := session.Must(session.NewSession(S3Config))
	downloader := s3manager.NewDownloader(sess)
	buf := aws.NewWriteAtBuffer([]byte{})
	_, err := downloader.Download(buf, &s3.GetObjectInput{
		Bucket: aws.String(Bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return nil, fmt.Errorf("failed to download tablet %s: %w", id, err)
	}
	var tablet *Tablet
	dec := gob.NewDecoder(bytes.NewBuffer(buf.Bytes()))
	err = dec.Decode(&tablet)
	if err != nil {
		return nil, fmt.Errorf("failed to decode tablet %s: %w", id, err)
	}
	return tablet, nil
}
