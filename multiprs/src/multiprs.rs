use std::{collections::HashSet, future::Future, net::TcpListener};

use nix::{
    sys::{
        signal::{kill, Signal},
        wait::{waitpid, WaitPidFlag},
    },
    unistd::{fork, ForkResult, Pid},
};
use signal_hook::{
    consts::{SIGCHLD, SIGINT, SIGQUIT, SIGTERM},
    iterator::{exfiltrator::WithOrigin, SignalsInfo},
    low_level::siginfo::{Origin, Process},
};
use tokio::runtime::Builder;

pub struct Server<FChild, Fut>
where
    FChild: Fn(u32, TcpListener) -> Fut,
    Fut: Future<Output = ()>,
{
    num_processes: u32,
    listener: TcpListener,
    child_init: FChild,
}

impl<FChild, Fut> Server<FChild, Fut>
where
    FChild: Fn(u32, TcpListener) -> Fut,
    Fut: Future<Output = ()>,
{
    pub fn from_listener(listener: TcpListener, child_init: FChild, num_processes: u32) -> Self {
        Self {
            num_processes,
            listener,
            child_init,
        }
    }

    pub fn fork(self) {
        let mut pids = HashSet::new();
        let mut is_parent = true;
        for child_num in 0..self.num_processes {
            let listener = clone_listener(&self.listener);
            let pid = self.fork_child(child_num, listener);
            if let Some(pid) = pid {
                pids.insert(pid);
            } else {
                is_parent = false;
                break;
            }
        }
        if is_parent {
            self.run_parent(pids)
        }
    }

    fn fork_child(&self, child_num: u32, listener: TcpListener) -> Option<Pid> {
        match unsafe { fork() }.expect("fork failed") {
            ForkResult::Parent { child } => {
                return Some(child);
            }
            ForkResult::Child => {
                let runtime = Builder::new_current_thread()
                    .enable_io()
                    .build()
                    .expect("cannot create runtime");
                runtime.block_on(async {
                    println!("child {child_num} started");
                    (self.child_init)(child_num, listener).await;
                });
                None
            }
        }
    }

    fn run_parent(&self, mut pids: HashSet<Pid>) {
        let mut signals = SignalsInfo::<WithOrigin>::new(&[SIGINT, SIGTERM, SIGQUIT, SIGCHLD])
            .expect("cannot register signal handlers");
        for signal in signals.forever() {
            match signal {
                Origin {
                    signal: SIGCHLD,
                    process,
                    ..
                } => wait_process(process, &mut pids),
                _ => break,
            }
        }
        for child in pids {
            // TODO: log, don't print
            if let Err(err) = kill(child, Signal::SIGKILL) {
                eprintln!("Kill failed: {err}");
            } else {
                println!("Reaped {child}")
            }
        }
    }
}

fn wait_process(process: Option<Process>, pids: &mut HashSet<Pid>) {
    let pid = process.map(|p| p.pid);
    let nixpid = pid.map(|p| nix::unistd::Pid::from_raw(p));
    if let Ok(status) = waitpid(nixpid, Some(WaitPidFlag::WNOHANG)) {
        let pid = pid.unwrap_or(-1);
        eprintln!("Child {pid} exited with status {status:?}");
        if let Some(nixpid) = nixpid {
            pids.remove(&nixpid);
        }
    }
}

fn clone_listener(listener: &TcpListener) -> TcpListener {
    listener.try_clone().expect("cannot clone listener")
}
