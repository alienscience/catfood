mod multiprs;

use std::{net::TcpListener, process};

use axum::{extract::State, routing::get, Router};

use crate::multiprs::Server;

async fn child(child_num: u32, listener: TcpListener) {
    let pid = process::id();
    let router = Router::new()
        .route(
            "/",
            get(|State((pid, child_num))| async move {
                format!("Hello from {child_num} with pid {pid}")
            }),
        )
        .with_state((pid, child_num));
    axum::Server::from_tcp(listener)
        .expect("cannot create server")
        .serve(router.into_make_service())
        .await
        .expect("cannot start server")
}

fn main() {
    let listener = TcpListener::bind("0.0.0.0:3000").expect("cannot bind to port");
    Server::from_listener(listener, child, 10).fork();
    println!("Parent exit");
}
