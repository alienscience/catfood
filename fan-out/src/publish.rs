use crossbeam_channel::{bounded, Receiver, Sender};
use enumflags2::{bitflags, BitFlags};
use std::{
    sync::{Arc, Condvar, Mutex},
    thread::{self, JoinHandle},
};

// TODO: remove bitflags completely
#[bitflags(default=Block)]
#[repr(u32)]
#[derive(Copy, Clone, PartialEq)]
pub enum PublishOptions {
    Block,
    Drop,
}

pub struct Publish<I: Iterator> {
    join_handle: JoinHandle<()>,
    routes: Routes<I::Item>,
}

impl<I> Publish<I>
where
    I: Iterator + Send + 'static,
    I::Item: Send,
{
    pub fn from_route<F>(iter: I, route_fn: F) -> Self
    where
        F: Fn(&I::Item) -> u16,
        F: Send + 'static,
    {
        let foreground_routes = Routes::new(BitFlags::<PublishOptions>::default());
        let background_routes = foreground_routes.clone();
        let jh = thread::spawn(move || run_publish(iter, route_fn, background_routes));
        Self {
            join_handle: jh,
            routes: foreground_routes,
        }
    }

    pub fn from_routable(iter: I) -> Self
    where
        I::Item: Routable,
    {
        Self::from_route(iter, |i| i.route())
    }

    pub fn block_without_listener(mut self) -> Self {
        self.routes.options |= PublishOptions::Block;
        self.routes.options.remove(PublishOptions::Drop);
        self
    }

    pub fn drop_without_listener(mut self) -> Self {
        self.routes.options |= PublishOptions::Drop;
        self.routes.options.remove(PublishOptions::Block);
        self
    }

    pub fn connect(&self, id: u16) -> Connected<I::Item> {
        let (s, r) = bounded(1);
        self.routes.add(id, s);
        Connected(r)
    }

    pub fn connect_routable(&self, example: &I::Item) -> Connected<I::Item>
    where
        I::Item: Routable,
    {
        self.connect(example.route())
    }

    pub fn join(self) -> std::thread::Result<()> {
        self.join_handle.join()
    }
}

pub trait Routable {
    fn route(&self) -> u16;
}

pub struct Connected<I>(Receiver<I>);

impl<I> Iterator for Connected<I> {
    type Item = I;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.recv().ok()
    }
}

struct Routes<I> {
    inner: Arc<(Mutex<RoutingTable<I>>, Condvar)>,
    options: BitFlags<PublishOptions>,
}

impl<I> Clone for Routes<I> {
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
            options: self.options,
        }
    }
}

impl<I> Routes<I> {
    fn new(options: BitFlags<PublishOptions>) -> Self {
        assert!(!options.contains(PublishOptions::Block | PublishOptions::Drop));
        let mutex = Mutex::new(RoutingTable::new());
        let cv = Condvar::new();
        let inner = Arc::new((mutex, cv));
        Self { inner, options }
    }

    fn get_entry<T, F>(&self, id: u16, extract_fn: F) -> Option<T>
    where
        F: FnOnce(&Entry<I>) -> T,
    {
        let (mutex, cv) = &*self.inner;
        let mut table = mutex.lock().unwrap();
        loop {
            let entry = table.get_entry(id);
            match entry {
                Some(e) => return Some(extract_fn(e)),
                None if self.options.contains(PublishOptions::Drop) => return None,
                _ => (),
            }
            // No such channel exists -- wait for a new channel to be added
            table = cv.wait(table).unwrap();
        }
    }

    fn get_sender(&self, id: u16) -> Option<Sender<I>> {
        self.get_entry(id, |p| p.0.clone())
    }

    fn add(&self, id: u16, sender: Sender<I>) {
        let (mutex, cv) = &*self.inner;
        let mut table = mutex.lock().unwrap();
        let entry = Entry(sender);
        table.add(id, entry);
        cv.notify_all();
    }

    fn close(&self) {
        let (mutex, _cv) = &*self.inner;
        let mut table = mutex.lock().unwrap();
        table.close();
    }
}

struct Entry<I>(Sender<I>);

impl<I> Clone for Entry<I> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

struct RoutingTable<I> {
    table: Vec<Option<Entry<I>>>,
    is_active: bool,
}

impl<I> RoutingTable<I> {
    fn new() -> Self {
        let table = Vec::new();
        Self {
            table,
            is_active: true,
        }
    }

    fn get_entry(&self, id: u16) -> Option<&Entry<I>> {
        let id = id as usize;
        self.table.get(id).and_then(|o| o.as_ref())
    }

    fn close(&mut self) {
        self.table.clear();
        self.is_active = false;
    }

    fn add(&mut self, id: u16, entry: Entry<I>) {
        if !self.is_active {
            return;
        }
        let id = id as usize;
        if id >= self.table.len() {
            self.table.resize(id + 1, None);
        }
        self.table[id] = Some(entry);
    }
}

fn run_publish<I, F>(iter: I, route_fn: F, routes: Routes<I::Item>)
where
    I: Iterator,
    F: Fn(&I::Item) -> u16,
{
    for i in iter {
        let dest_id = route_fn(&i);
        if let Some(sender) = routes.get_sender(dest_id) {
            sender.send(i).unwrap();
        }
    }
    routes.close();
}
