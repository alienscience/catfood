use std::thread::{self, JoinHandle};

pub trait Consume<T>: Sized {
    fn background<F, R>(self, target_fn: F) -> JoinHandle<R>
    where
        F: FnOnce(Self) -> R,
        F: Send + 'static,
        R: Send + 'static;
}

impl<T, I> Consume<T> for I
where
    I: Iterator<Item = T> + Send + 'static,
{
    fn background<F, R>(self, target_fn: F) -> JoinHandle<R>
    where
        F: FnOnce(Self) -> R,
        F: Send + 'static,
        R: Send + 'static,
    {
        thread::spawn(move || target_fn(self))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn consume_iter() {
        let iter = 0..10;
        let jh = iter.background(|i| i.collect::<Vec<_>>());
        let res = jh.join().unwrap();
        assert_eq!(res, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
    }
}
