mod consume;
mod publish;

pub use crate::{
    consume::Consume,
    publish::{Connected, Publish, PublishOptions, Routable},
};

#[cfg(test)]
mod tests {
    use super::*;

    use std::{thread, time::Duration};

    macro_rules! partial {
        ($extract:pat => $res:expr) => {
            |arg| match arg {
                $extract => $res,
                _ => unreachable!(),
            }
        };
    }
    fn collect<I: Iterator>(iter: I) -> Vec<I::Item> {
        iter.collect()
    }

    #[test]
    fn fizz_buzz() {
        let iter = 0..10;
        let publish = Publish::from_route(iter, |i| {
            if *i > 0 && i % 3 == 0 {
                1
            } else if *i > 0 && i % 5 == 0 {
                2
            } else {
                0
            }
        });
        let numbers = publish.connect(0).background(collect);
        let fizz = publish.connect(1).background(collect);
        let buzz = publish.connect(2).background(collect);
        let numbers = numbers.join().unwrap();
        let fizz = fizz.join().unwrap();
        let buzz = buzz.join().unwrap();
        assert_eq!(numbers, [0, 1, 2, 4, 7, 8]);
        assert_eq!(fizz, [3, 6, 9]);
        assert_eq!(buzz, [5]);
        publish.join().unwrap()
    }

    #[derive(Debug, PartialEq)]
    enum Message {
        Start(u32),
        Stop,
    }

    impl Routable for Message {
        fn route(&self) -> u16 {
            match self {
                Message::Start(_) => 0,
                _ => 1,
            }
        }
    }

    #[test]
    fn message_bus() {
        let messages = vec![
            Message::Start(1),
            Message::Start(2),
            Message::Start(3),
            Message::Stop,
        ];
        let bus = Publish::from_routable(messages.into_iter());
        let start = bus.connect_routable(&Message::Start(0)).background(|iter| {
            iter.map(partial!(Message::Start(n) => n))
                .collect::<Vec<_>>()
        });
        let stop = bus.connect_routable(&Message::Stop).background(collect);
        let start = start.join().unwrap();
        let stop = stop.join().unwrap();
        assert_eq!(start, [1, 2, 3]);
        assert_eq!(stop, [Message::Stop]);
    }

    #[test]
    fn drop_items() {
        let iter = 0..100;
        let bus = Publish::from_route(iter, |_| 1).drop_without_listener();
        thread::sleep(Duration::from_millis(100));
        let jh = bus.connect(1).background(collect);
        let res = jh.join().unwrap();
        let all: Vec<_> = (0..100).collect();
        assert_ne!(res, all);
    }
}
