#[derive(Clone)]
enum Entry<K, V, const L: usize>
where
    K: Clone,
    V: Clone,
{
    None,
    Small { len: u8, k: K, v: [V; L] },
    Large(Vec<(K, Vec<V>)>),
}

struct IntMultiMap<K, V, const L: usize>
where
    K: Clone,
    V: Clone,
{
    table: Vec<Entry<K, V, L>>,
}

impl<K, V, const L: usize> IntMultiMap<K, V, L>
where
    K: Clone,
    V: Clone,
{
    fn new(size: usize) -> Self {
        let table = vec![Entry::None; size];
        Self { table }
    }
}

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use std::mem::size_of;

    use super::*;

    #[test]
    fn it_works() {
        let ze = size_of::<Entry<4, u32, u32>>();
        assert!(false, "Size of entry: {ze} bytes");
    }
}
