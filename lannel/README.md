
# Connect Local and Remote with Channels

```go
local := NewLocal()
remote := NewRemote(url)
conn := Connect(local, remote)
local.StartSomething()
```
