package main

import (
	"log"
	"math/rand"
	"time"
)

type Endpoint interface {
	Channels() (chan<- []byte, <-chan []byte)
}

type LocalPing struct {
	send    chan []byte
	receive chan []byte
}

type RemotePong struct {
	send    chan []byte
	receive chan []byte
}

func (l *LocalPing) Channels() (chan<- []byte, <-chan []byte) {
	return l.receive, l.send
}

func (l *LocalPing) Run() {
	for {
		// Simulate work
		delay := rand.Intn(5)
		time.Sleep(time.Duration(delay) * time.Second)
		select {
		case msg := <-l.receive:
			log.Print("Got", msg)
		case l.send <- []byte("Ping"):
			// Send successful
		}
	}
}

func (r *RemotePong) Channels() (chan<- []byte, <-chan []byte) {
	return r.receive, r.send
}

func main() {
	local := &LocalPing{make(chan []byte), make(chan []byte)}
	remote := &RemotePong{make(chan []byte), make(chan []byte)}
}
