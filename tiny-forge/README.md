
# Tiny Forge

A minimal cloud native git forge.


# All Features

- Configured from git.
    -  e.g The `/~user/infra` repo is configured by the `/~user/.config/infra` repo.
    -  e.g Global forge configuration in the `/forge/.config` repo.
- View git repositories.
- Webhooks on push.
- PRs.
- Uses an external CI.
	- Woodpecker?
- Uses external auth.
	- Dex?
    - Write own "tiny-auth"?
- Sends email notifications.
