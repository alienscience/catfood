use cmake::Config;
use std::{env, path::PathBuf};

fn main() {
    let dst = Config::new("vendor").build();

    println!("cargo:rustc-link-search=native={}/lib", dst.display());
    println!("cargo:rustc-link-lib=static=signal-protocol-c");

    let header = format!("{}/include/signal/signal_protocol.h", dst.display());
    let bindings = bindgen::Builder::default()
        .header(header)
        .generate()
        .expect("Unable to generate Rust bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").expect("Cannot get output directory"));
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Could not write bindings");
}
