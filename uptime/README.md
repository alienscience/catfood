
# Check uptime

- Uptime Kuma: https://github.com/louislam/uptime-kuma
        - Can send alerts over Signal
        - Need very cheap VPS

## Self Written

- Maybe use Signal to send the alerts (no apps needed).
        - C and Java APIs
                - Could use Rust to wrap: https://github.com/signalapp/libsignal-protocol-c
                        - How to embed C in a Rust crate: https://docs.rust-embedded.org/book/interoperability/c-with-rust.html
                        - Tips: https://kornel.ski/rust-sys-crate
                - C example: https://github.com/gkdr/axc
                - Need to add a keystore, could use Pickle DB: https://docs.rs/pickledb/latest/pickledb/
                - There is also an AGPL library: https://github.com/whisperfish/libsignal-service-rs (no docs)
- Alternative to Signal: Flutter app and Android Notifications
- Have scheduled downtime
- DNS cache busting (ignore TTL and go to authorative server)

