mod compress;
mod debug;
mod decompress;

fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod tests {
    use std::{
        io::{Cursor, Seek},
        iter,
    };

    use quickcheck::TestResult;
    use quickcheck_macros::quickcheck;

    use crate::{compress::compress_block, decompress::decompress_block};

    #[quickcheck]
    fn round_trip_block(input: Vec<u8>) -> TestResult {
        if input.len() > 1024 * 1024 {
            return TestResult::discard();
        }
        let mut compressed = Cursor::new(Vec::new());
        let mut decompressed = Vec::new();
        compress_block(&mut compressed, &input);
        println!("Compressed len: {}", compressed.position());
        compressed.rewind().unwrap();
        let block_len = compressed.get_ref().len();
        decompress_block(&mut compressed, &mut decompressed, block_len);
        TestResult::from_bool(input == decompressed)
    }

    #[quickcheck]
    fn round_trip_compress_block(pattern: Vec<u8>) -> TestResult {
        if pattern.len() > 1024 * 1024 {
            return TestResult::discard();
        }
        let input: Vec<_> = iter::repeat(pattern)
            .take(4)
            .flat_map(Vec::into_iter)
            .collect();
        let mut compressed = Cursor::new(Vec::new());
        let mut decompressed = Vec::new();
        compress_block(&mut compressed, &input);
        println!("Compressed len: {}", compressed.position());
        compressed.rewind().unwrap();
        let block_len = compressed.get_ref().len();
        decompress_block(&mut compressed, &mut decompressed, block_len);
        TestResult::from_bool(input == decompressed)
    }
}
