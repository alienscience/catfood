use std::{cmp::min, io::Read};

struct Sequence {
    literal: Vec<u8>,
    end_of_block: bool,
    offset: u16,
    match_length: u32,
    bytes_read: usize,
}

pub fn decompress_block<R>(rdr: &mut R, window: &mut Vec<u8>, block_size: usize)
where
    R: Read,
{
    let mut remaining = block_size;
    loop {
        let mut seq = read_sequence(rdr, remaining);
        window.append(&mut seq.literal);
        println!("Window: {window:?}");
        if seq.end_of_block {
            return;
        }
        println!("Window length: {}, Offset: {}", window.len(), seq.offset);
        assert!(seq.offset > 0);
        assert!((seq.offset as usize) <= window.len());
        let start = window.len() - seq.offset as usize;
        let match_len = seq.match_length as usize;
        overlapping_copy(window, start, match_len);
        println!("Window: {window:?}");
        remaining -= seq.bytes_read;
    }
}

fn overlapping_copy(window: &mut Vec<u8>, start: usize, mut match_len: usize) {
    while match_len > 0 {
        println!("Match length: {match_len}");
        let non_overlap_len = min(match_len, window.len() - start);
        window.extend_from_within(start..(start + non_overlap_len));
        match_len -= non_overlap_len;
    }
}

fn read_sequence<R>(rdr: &mut R, remaining: usize) -> Sequence
where
    R: Read,
{
    // Read first byte.
    let mut token_buf = [0u8];
    if let Err(_) = rdr.read_exact(&mut token_buf) {
        // TODO: Sequence::default()
        return Sequence {
            literal: Vec::new(),
            end_of_block: true,
            offset: 0,
            match_length: 0,
            bytes_read: 0,
        };
    }
    let mut bytes_read = 1;
    let match_len_first = token_buf[0] & 0x0f;
    let literal_first = (token_buf[0] & 0xf0) >> 4;
    let (literal_len, num_bytes) = if literal_first == 0xf {
        read_length(rdr, 0xf)
    } else {
        (literal_first as u32, 0)
    };
    bytes_read += num_bytes;
    println!("Literal len: {literal_len}");
    // Read literal.
    let mut literal = vec![0; literal_len as usize];
    rdr.read_exact(&mut literal).unwrap();
    bytes_read += literal.len();
    println!("Bytes read: {bytes_read}, Remaining: {remaining}");
    if bytes_read >= remaining {
        return Sequence {
            literal,
            end_of_block: true,
            offset: 0,
            match_length: 0,
            bytes_read,
        };
    }
    let mut offset_buf = [0u8, 0u8];
    // Read offset + match length
    rdr.read_exact(&mut offset_buf).unwrap();
    bytes_read += 2;
    println!("Offset Buf: {offset_buf:?}");
    let offset = u16::from_le_bytes(offset_buf);
    println!("First Match len nibble: {match_len_first}");
    let (match_length, num_bytes) = if match_len_first == 0xf {
        read_length(rdr, 0x13)
    } else {
        (match_len_first as u32 + 0x04, 0)
    };
    bytes_read += num_bytes;
    Sequence {
        literal,
        end_of_block: false,
        offset,
        match_length,
        bytes_read,
    }
}

// Returns (length, num_bytes_read)
fn read_length<R>(rdr: &mut R, start: u32) -> (u32, usize)
where
    R: Read,
{
    let mut current = start;
    let mut buf = [0u8];
    for i in 0..4 {
        rdr.read_exact(&mut buf).unwrap();
        let next_byte = buf[0];
        let next_byte = next_byte as u32;
        println!("next byte: {next_byte}, current: {current}");
        assert!(next_byte < 0xffff - current, "length > 32 bits");
        current += next_byte;
        if next_byte < 0xff {
            return (current, i + 1);
        }
    }
    assert!(false, "length > 32 bits");
    (0, 0)
}

#[cfg(test)]
mod tests {
    use super::decompress_block;

    #[test]
    fn simple_sequence() {
        // 48 byte literal + 20 byte copy
        let mut input: Vec<u8> = vec![0xff, 0x21];
        for i in 0..48 {
            input.push(i);
        }
        let offset = (0x14 as u16).to_le_bytes();
        input.extend_from_slice(&offset);
        input.push(0x01);
        // 5 bytes literal only to finish.
        input.push(0x50);
        for i in 0..5 {
            input.push(i);
        }
        let mut win = Vec::new();
        decompress_block(&mut input.as_slice(), &mut win, input.len());
        assert_eq!(win.len(), 48 + 20 + 5);
    }

    #[test]
    fn long_literal() {
        // 280 byte literal + 4 byte copy
        let mut input: Vec<u8> = vec![0xf0, 0xff, 0x0a];
        for i in 0..280 {
            input.push((i % 0xff) as u8);
        }
        let offset = (0x04 as u16).to_le_bytes();
        input.extend_from_slice(&offset);
        // 5 bytes literal only to finish.
        input.push(0x50);
        for i in 0..5 {
            input.push(i);
        }
        let mut win = Vec::new();
        decompress_block(&mut input.as_slice(), &mut win, input.len());
        assert_eq!(win.len(), 280 + 4 + 5);
    }

    #[test]
    fn with_overlap() {
        // 1 byte literal + 128 byte copy
        let mut input: Vec<u8> = vec![0x1f, 0x11];
        let offset = (0x01 as u16).to_le_bytes();
        input.extend_from_slice(&offset);
        input.push(0x6d);
        // 5 bytes literal only to finish.
        input.push(0x50);
        for _ in 0..5 {
            input.push(0x11);
        }
        let mut win = Vec::new();
        decompress_block(&mut input.as_slice(), &mut win, input.len());
        assert_eq!(win.len(), 1 + 128 + 5);
        assert!(win.into_iter().all(|b| b == 0x11));
    }

    #[test]
    fn small_buffer() {
        // 12 byte literal
        let input: Vec<u8> = vec![0x30, 0x00, 0x00, 0x00];
        let mut win = Vec::new();
        decompress_block(&mut input.as_slice(), &mut win, input.len());
        assert_eq!(win.len(), 3);
        assert!(win.into_iter().all(|b| b == 0x00));
    }
}
