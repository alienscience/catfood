use std::{collections::HashMap, io::Write};

use crate::debug::debug;

pub fn compress_block<W>(wr: &mut W, data: &[u8])
where
    W: Write,
{
    if data.is_empty() {
        return;
    }
    if data.len() <= 5 {
        output_block_end(wr, &data);
        return;
    }
    let mut dict = HashMap::with_capacity(data.len());
    let mut i = 0;
    let mut literal_len = 0;
    while i + 5 < data.len() {
        let four_bytes: [u8; 4] = data[i..(i + 4)].try_into().unwrap();
        let key = u32::from_le_bytes(four_bytes);
        if let Some(previous) = dict.get(&key).copied() {
            dict.insert(key, i);
            let match_len = get_match_len(data, previous, i);
            output_sequence(wr, &data[(i - literal_len)..i], i - previous, match_len);
            i += match_len;
            literal_len = 0;
        } else {
            dict.insert(key, i);
            literal_len += 1;
            i += 1;
        }
    }
    output_block_end(wr, &data[(i - literal_len)..]);
}

fn get_match_len(data: &[u8], previous: usize, start: usize) -> usize {
    let mut match_len = 4;
    while start + match_len + 5 < data.len()
        && data[previous + match_len] == data[start + match_len]
    {
        match_len += 1;
    }
    debug!(
        "previous: {previous}, start: {start}, len= {}, fail_on: {} != {} @ {}",
        data.len(),
        data[previous + match_len],
        data[start + match_len],
        start + match_len
    );
    match_len
}

fn output_sequence<W>(wr: &mut W, literal: &[u8], offset: usize, match_len: usize)
where
    W: Write,
{
    debug!(
        "Output sequence, literal_len={}, offset={}, match_len={}",
        literal.len(),
        offset,
        match_len
    );
    // Prepare the sequence
    let mut remaining_literal_len = literal.len();
    let mut remaining_match_len = match_len - 4;
    let mut literal_len_buf: Option<Vec<u8>> = None;
    let mut match_len_buf: Option<Vec<u8>> = None;
    let literal_nibble: u8 = if remaining_literal_len < 0x0f {
        (remaining_literal_len & 0x0f) as u8
    } else {
        remaining_literal_len -= 0x0f;
        literal_len_buf = Some(length_as_buf(remaining_literal_len));
        0x0f
    };
    let match_nibble: u8 = if remaining_match_len < 0x0f {
        (remaining_match_len & 0x0f) as u8
    } else {
        remaining_match_len -= 0x0f;
        match_len_buf = Some(length_as_buf(remaining_match_len));
        0x0f
    };
    let first_byte = (literal_nibble << 4) | match_nibble;
    // Write the sequence
    let first_byte_buf = [first_byte];
    wr.write_all(&first_byte_buf).unwrap();
    if let Some(buf) = literal_len_buf {
        wr.write_all(&buf).unwrap();
    }
    wr.write_all(literal).unwrap();
    let offset_buf = (offset as u16).to_le_bytes();
    wr.write_all(&offset_buf).unwrap();
    if let Some(buf) = match_len_buf {
        wr.write_all(&buf).unwrap();
    }
}

fn output_block_end<W>(wr: &mut W, literal: &[u8])
where
    W: Write,
{
    debug!("Output end, literal_len={}", literal.len());
    // Prepare the sequence
    let mut remaining_literal_len = literal.len();
    let mut literal_len_buf: Option<Vec<u8>> = None;
    let literal_nibble: u8 = if remaining_literal_len < 0x0f {
        (remaining_literal_len & 0x0f) as u8
    } else {
        remaining_literal_len -= 0x0f;
        literal_len_buf = Some(length_as_buf(remaining_literal_len));
        0x0f
    };
    let first_byte = literal_nibble << 4;
    // Write the sequence
    let first_byte_buf = [first_byte];
    wr.write_all(&first_byte_buf).unwrap();
    if let Some(buf) = literal_len_buf {
        wr.write_all(&buf).unwrap();
    }
    wr.write_all(literal).unwrap();
}

fn length_as_buf(mut len: usize) -> Vec<u8> {
    let mut ret = Vec::new();
    loop {
        if len < 0xff {
            ret.push(len as u8);
            break;
        } else {
            ret.push(0xff);
            len -= 0xff;
        }
    }
    ret
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::iter;

    #[test]
    fn compress_mixed() {
        let mut output = Vec::new();
        let input: Vec<u8> = (0..0xff)
            .into_iter()
            .chain(iter::repeat(0x00).take(21))
            .collect();
        compress_block(&mut output, &input);
        debug!("{output:?}");
        assert_eq!(output[0], 0xfb); // Big literal & 21 - 5 - 4 match length.
        assert_eq!(output[1], 0xf1); // 255 +1 - 15 remaining literal length.
        assert_eq!(output[258], 0x01); // 1 byte offset, little endian u16
        assert_eq!(output[259], 0x00);
        assert_eq!(output[260], 0x50); // 5 byte literal sequence
        assert_eq!(output[261], 0x00);
        assert_eq!(output[262], 0x00);
        assert_eq!(output[263], 0x00);
        assert_eq!(output[264], 0x00);
        assert_eq!(output[265], 0x00);
    }

    #[test]
    fn repeating_pattern() {
        let mut output = Vec::new();
        let pattern: Vec<u8> = (0..0x32).into_iter().collect();
        let mut input = Vec::new();
        for _ in 0..16 {
            input.extend_from_slice(&pattern);
        }
        compress_block(&mut output, &input);
        debug!("{output:?}");
        assert_eq!(output[0], 0xff); // Big literal & Big match
        assert_eq!(output[1], 0x23); // 50 - 15 remaining literal length
        assert_eq!(output[52], 0x32); // 50 byte offset, little endian u16
        assert_eq!(output[53], 0x00);
        assert_eq!(output[54], 0xff); // 800 - 50 - 5 - 19 match length
        assert_eq!(output[55], 0xff); // 471 match length
        assert_eq!(output[56], 0xd8); // 216 match length
        assert_eq!(output[57], 0x50); // 5 byte literal sequence
        assert_eq!(output[58], 0x2d);
        assert_eq!(output[59], 0x2e);
        assert_eq!(output[60], 0x2f);
        assert_eq!(output[61], 0x30);
        assert_eq!(output[62], 0x31);
    }

    #[test]
    fn small_buffer() {
        let input: Vec<u8> = iter::repeat(0x00).take(12).collect();
        let mut output = Vec::new();
        compress_block(&mut output, &input);
        debug!("{output:?}");
        assert_eq!(output[0], 0x12); // 1 byte literal & 6 bytes match
        assert_eq!(output[1], 0x00); // 1 byte literal
        assert_eq!(output[2], 0x01); // 1 byte offset, little endian u16
        assert_eq!(output[3], 0x00);
        // End block
        assert_eq!(output[4], 0x50); // 5 byte literal
        assert_eq!(output[5], 0x00); // Literal
        assert_eq!(output[6], 0x00);
        assert_eq!(output[7], 0x00);
        assert_eq!(output[8], 0x00);
        assert_eq!(output[9], 0x00);
        assert_eq!(10, output.len());
    }
}
