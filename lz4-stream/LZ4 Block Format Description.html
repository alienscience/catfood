<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>LZ4 Block Format Description</title><link rel="stylesheet" type="text/css" href="LZ4%20Block%20Format%20Description_files/base.css"><link rel="stylesheet" type="text/css" href="LZ4%20Block%20Format%20Description_files/doc.css"><link rel="stylesheet" type="text/css" href="LZ4%20Block%20Format%20Description_files/prettify.css"><!-- default customHeadTagPart --></head><body class="Site search_plugin_added"><header class="Site-header "><div class="Header"><div class="Header-title"></div></div></header><div class="Site-content Site-Content--markdown"><div class="Container"><div class="doc"><h1><a class="h" name="LZ4-Block-Format-Description" href="#LZ4-Block-Format-Description"><span></span></a><a class="h" name="lz4-block-format-description" href="#lz4-block-format-description"><span></span></a>LZ4 Block Format Description</h1><p>Last revised: 2022-07-31 . Author : Yann Collet</p><p>This
 specification is intended for developers willing to produce or read LZ4
 compressed data blocks using any programming language of their choice.</p><p>LZ4
 is an LZ77-type compressor with a fixed byte-oriented encoding format. 
There is no entropy encoder back-end nor framing layer. The latter is 
assumed to be handled by other parts of the system (see <a href="https://android.googlesource.com/platform/external/lz4/+/HEAD/doc/lz4_Frame_format.md">LZ4 Frame format</a>). This design is assumed to favor simplicity and speed.</p><p>This
 document describes only the Block Format, not how the compressor nor 
decompressor actually work. For more details on such topics, see later 
section “Implementation Notes”.</p><h2><a class="h" name="Compressed-block-format" href="#Compressed-block-format"><span></span></a><a class="h" name="compressed-block-format" href="#compressed-block-format"><span></span></a>Compressed block format</h2><p>An
 LZ4 compressed block is composed of sequences. A sequence is a suite of
 literals (not-compressed bytes), followed by a match copy operation.</p><p>Each sequence starts with a <code class="code">token</code>. The <code class="code">token</code> is a one byte value, separated into two 4-bits fields. Therefore each field ranges from 0 to 15.</p><p>The first field uses the 4 high-bits of the token. It provides the length of literals to follow.</p><p>If
 the field value is smaller than 15, then it represents the total nb of 
literals present in the sequence, including 0, in which case there is no
 literal.</p><p>The value 15 is a special case: more bytes are required 
to indicate the full length. Each additional byte then represents a 
value from 0 to 255, which is added to the previous value to produce a 
total length. When the byte value is 255, another byte must be read and 
added, and so on. There can be any number of bytes of value <code class="code">255</code> following <code class="code">token</code>.
 The Block Format does not define any “size limit”, though real 
implementations may feature some practical limits (see more details in 
later chapter “Implementation Notes”).</p><p>Note : this format explains why a non-compressible input block is expanded by 0.4%.</p><p>Example 1 : A literal length of 48 will be represented as :</p><ul><li>15 : value for the 4-bits High field</li><li>33 : (=48-15) remaining length to reach 48</li></ul><p>Example 2 : A literal length of 280 will be represented as :</p><ul><li>15  : value for the 4-bits High field</li><li>255 : following byte is maxed, since 280-15 &gt;= 255</li><li>10  : (=280 - 15 - 255) remaining length to reach 280</li></ul><p>Example 3 : A literal length of 15 will be represented as :</p><ul><li>15 : value for the 4-bits High field</li><li>0  : (=15-15) yes, the zero must be output</li></ul><p>Following <code class="code">token</code>
 and optional length bytes, are the literals themselves. They are 
exactly as numerous as just decoded (length of literals). Reminder: it's
 possible that there are zero literals.</p><p>Following the literals is the match copy operation.</p><p>It starts by the <code class="code">offset</code> value. This is a 2 bytes value, in little endian format (the 1st byte is the “low” byte, the 2nd one is the “high” byte).</p><p>The <code class="code">offset</code> represents the position of the match to be copied from the past. For example, 1 means “current position - 1 byte”. The maximum <code class="code">offset</code> value is 65535. 65536 and beyond cannot be coded. Note that 0 is an invalid <code class="code">offset</code> value. The presence of a 0 <code class="code">offset</code> value denotes an invalid (corrupted) block.</p><p>Then the <code class="code">matchlength</code> can be extracted. For this, we use the second <code class="code">token</code>
 field, the low 4-bits. Such a value, obviously, ranges from 0 to 15. 
However here, 0 means that the copy operation is minimal. The minimum 
length of a match, called <code class="code">minmatch</code>, is 4. As a
 consequence, a 0 value means 4 bytes. Similarly to literal length, any 
value smaller than 15 represents a length, to which 4 (<code class="code">minmatch</code>)
 must be added, thus ranging from 4 to 18. A value of 15 is special, 
meaning 19+ bytes, to which one must read additional bytes, one at a 
time, with each byte value ranging from 0 to 255. They are added to 
total to provide the final match length. A 255 value means there is 
another byte to read and add. There is no limit to the number of 
optional <code class="code">255</code> bytes that can be present, and 
therefore no limit to representable match length, though real-life 
implementations are likely going to enforce limits for practical reasons
 (see more details in “Implementation Notes” section below).</p><p>Note: this format has a maximum achievable compression ratio of about ~250.</p><p>Decoding the <code class="code">matchlength</code> reaches the end of current sequence. Next byte will be the start of another sequence, and therefore a new <code class="code">token</code>.</p><h2><a class="h" name="End-of-block-conditions" href="#End-of-block-conditions"><span></span></a><a class="h" name="end-of-block-conditions" href="#end-of-block-conditions"><span></span></a>End of block conditions</h2><p>There are specific restrictions required to terminate an LZ4 block.</p><ol><li>The last sequence contains only literals. The block ends right after the literals (no <code class="code">offset</code> field).</li><li>The last 5 bytes of input are always literals. Therefore, the last sequence contains at least 5 bytes.<ul><li>Special
 : if input is smaller than 5 bytes, there is only one sequence, it 
contains the whole input as literals. Even empty input can be 
represented, using a zero byte, interpreted as a final token without 
literal and without a match.</li></ul></li><li>The last match must start at least 12 bytes before the end of block. The last match is part of the <em>penultimate</em> sequence. It is followed by the last sequence, which contains <em>only</em> literals.<ul><li>Note that, as a consequence, blocks &lt; 12 bytes cannot be compressed. And as an extension, <em>independent</em>
 blocks &lt; 13 bytes cannot be compressed, because they must start by 
at least one literal, that the match can then copy afterwards.</li></ul></li></ol><p>When a block does not respect these end conditions, a conformant decoder is allowed to reject the block as incorrect.</p><p>These
 rules are in place to ensure compatibility with a wide range of 
historical decoders which rely on these conditions for their 
speed-oriented design.</p><h2><a class="h" name="Implementation-notes" href="#Implementation-notes"><span></span></a><a class="h" name="implementation-notes" href="#implementation-notes"><span></span></a>Implementation notes</h2><p>The
 LZ4 Block Format only defines the compressed format, it does not tell 
how to create a decoder or an encoder, which design is left free to the 
imagination of the implementer.</p><p>However, thanks to experience, 
there are a number of typical topics that most implementations will have
 to consider. This section tries to provide a few guidelines.</p><h4><a class="h" name="Metadata" href="#Metadata"><span></span></a><a class="h" name="metadata" href="#metadata"><span></span></a>Metadata</h4><p>An
 LZ4-compressed Block requires additional metadata for proper decoding. 
Typically, a decoder will require the compressed block‘s size, and an 
upper bound of decompressed size. Other variants exist, such as knowing 
the decompressed size, and having an upper bound of the input size. The 
Block Format does not specify how to transmit such information, which is
 considered an out-of-band information channel. That’s because in many 
cases, the information is present in the environment. For example, 
databases must store the size of their compressed block for indexing, 
and know that their decompressed block can't be larger than a certain 
threshold.</p><p>If you need a format which is “self-contained”, and 
also transports the necessary metadata for proper decoding on any 
platform, consider employing the <a href="https://android.googlesource.com/platform/external/lz4/+/HEAD/doc/lz4_Frame_format.md">LZ4 Frame format</a> instead.</p><h4><a class="h" name="Large-lengths" href="#Large-lengths"><span></span></a><a class="h" name="large-lengths" href="#large-lengths"><span></span></a>Large lengths</h4><p>While
 the Block Format does not define any maximum value for length fields, 
in practice, most implementations will feature some form of limit, since
 it's expected for such values to be stored into registers of fixed bit 
width.</p><p>If length fields use 64-bit registers, then it can be 
assumed that there is no practical limit, as it would require a single 
continuous block of multiple petabytes to reach it, which is 
unreasonable by today's standard.</p><p>If length fields use 32-bit 
registers, then it can be overflowed, but requires a compressed block of
 size &gt; 16 MB. Therefore, implementations that do not deal with 
compressed blocks &gt; 16 MB are safe. However, if such a case is 
allowed, then it's recommended to check that no large length overflows 
the register.</p><p>If length fields use 16-bit registers, then it's 
definitely possible to overflow such register, with less than &lt; 300 
bytes of compressed data.</p><p>A conformant decoder should be able to 
detect length overflows when it‘s possible, and simply error out when 
that happens. The input block might not be invalid, it’s just not 
decodable by the local decoder implementation.</p><p>Note that, in order
 to be compatible with the larger LZ4 ecosystem, it's recommended to be 
able to read and represent lengths of up to 4 MB, and to accept blocks 
of size up to 4 MB. Such limits are compatible with 32-bit length 
registers, and prevent overflow of 32-bit registers.</p><h4><a class="h" name="Safe-decoding" href="#Safe-decoding"><span></span></a><a class="h" name="safe-decoding" href="#safe-decoding"><span></span></a>Safe decoding</h4><p>If
 a decoder receives compressed data from any external source, it is 
recommended to ensure that the decoder is resilient to corrupted input, 
and made safe from buffer overflow manipulations. Always ensure that 
read and write operations remain within the limits of provided buffers.</p><p>Of
 particular importance, ensure that the nb of bytes instructed to copy 
does not overflow neither the input nor the output buffers. Ensure also,
 when reading an offset value, that the resulting position to copy does 
not reach beyond the beginning of the buffer. Such a situation can 
happen during the first 64 KB of decoded data.</p><p>For more safety, 
test the decoder with fuzzers to ensure it's resilient to improbable 
sequences of conditions. Combine them with sanitizers, in order to catch
 overflows (asan) or initialization issues (msan).</p><p>Pay some 
attention to offset 0 scenario, which is invalid, and therefore must not
 be blindly decoded: a naive implementation could preserve destination 
buffer content, which could then result in information disclosure if 
such buffer was uninitialized and still containing private data. For 
reference, in such a scenario, the reference LZ4 decoder clears the 
match segment with <code class="code">0</code> bytes, though other solutions are certainly possible.</p><p>Finally, pay attention to the “overlap match” scenario, when <code class="code">matchlength</code> is larger than <code class="code">offset</code>. In which case, since <code class="code">match_pos + matchlength &gt; current_pos</code>,
 some of the later bytes to copy do not exist yet, and will be generated
 during the early stage of match copy operation. Such scenario must be 
handled with special care. A common case is an offset of 1, meaning the 
last byte is repeated <code class="code">matchlength</code> times.</p><h4><a class="h" name="Compression-techniques" href="#Compression-techniques"><span></span></a><a class="h" name="compression-techniques" href="#compression-techniques"><span></span></a>Compression techniques</h4><p>The
 core of a LZ4 compressor is to detect duplicated data across past 64 
KB. The format makes no assumption nor limits to the way a compressor 
searches and selects matches within the source data block. For example, 
an upper compression limit can be reached, using a technique called 
“full optimal parsing”, at high cpu and memory cost. But multiple other 
techniques can be considered, featuring distinct time / performance 
trade-offs. As long as the specified format is respected, the result 
will be compatible with and decodable by any compliant decoder.</p></div></div></div><!-- default customFooter --><footer class="Site-footer"><div class="Footer"><span class="Footer-poweredBy">Powered by <a href="https://gerrit.googlesource.com/gitiles/">Gitiles</a>| <a href="https://policies.google.com/privacy">Privacy</a>| <a href="https://policies.google.com/terms">Terms</a></span><div class="Footer-links"></div></div></footer></body></html>