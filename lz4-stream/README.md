
# Streaming LZ4

Writes blocks to a writer:
```
Data -> CompressBlock -> WriteBlock -> Writer
          ^
          `- Dict
```

Reads blocks from a reader:
```
Reader -> BufReader -> DecodeBlock -> DecompressBlock -> Vec
```

## Offsets

Offsets are maximum 16bits (65535).

## TODO

- Cannot detect end of block without knowing a block length.
    - Need a simple framing format
