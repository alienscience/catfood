<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>LZ4 Frame Format Description</title><link rel="stylesheet" type="text/css" href="LZ4%20Frame%20Format%20Description_files/base.css"><link rel="stylesheet" type="text/css" href="LZ4%20Frame%20Format%20Description_files/doc.css"><link rel="stylesheet" type="text/css" href="LZ4%20Frame%20Format%20Description_files/prettify.css"><!-- default customHeadTagPart --></head><body class="Site search_plugin_added"><header class="Site-header "><div class="Header"><div class="Header-title"></div></div></header><div class="Site-content Site-Content--markdown"><div class="Container"><div class="doc"><h1><a class="h" name="LZ4-Frame-Format-Description" href="#LZ4-Frame-Format-Description"><span></span></a><a class="h" name="lz4-frame-format-description" href="#lz4-frame-format-description"><span></span></a>LZ4 Frame Format Description</h1><h3><a class="h" name="Notices" href="#Notices"><span></span></a><a class="h" name="notices" href="#notices"><span></span></a>Notices</h3><p>Copyright (c) 2013-2020 Yann Collet</p><p>Permission
 is granted to copy and distribute this document for any purpose and 
without charge, including translations into other languages and 
incorporation into compilations, provided that the copyright notice and 
this notice are preserved, and that any substantive changes or deletions
 from the original are clearly marked. Distribution of this document is 
unlimited.</p><h3><a class="h" name="Version" href="#Version"><span></span></a><a class="h" name="version" href="#version"><span></span></a>Version</h3><p>1.6.2 (12/08/2020)</p><h2><a class="h" name="Introduction" href="#Introduction"><span></span></a><a class="h" name="introduction" href="#introduction"><span></span></a>Introduction</h2><p>The
 purpose of this document is to define a lossless compressed data 
format, that is independent of CPU type, operating system, file system 
and character set, suitable for File compression, Pipe and streaming 
compression using the <a href="http://www.lz4.org/">LZ4 algorithm</a>.</p><p>The
 data can be produced or consumed, even for an arbitrarily long 
sequentially presented input data stream, using only an a priori bounded
 amount of intermediate storage, and hence can be used in data 
communications. The format uses the LZ4 compression method, and optional
 <a href="https://github.com/Cyan4973/xxHash">xxHash-32 checksum method</a>, for detection of data corruption.</p><p>The data format defined by this specification does not attempt to allow random access to compressed data.</p><p>This
 specification is intended for use by implementers of software to 
compress data into LZ4 format and/or decompress data from LZ4 format. 
The text of the specification assumes a basic background in programming 
at the level of bits and other primitive data representations.</p><p>Unless
 otherwise indicated below, a compliant compressor must produce data 
sets that conform to the specifications presented here. It doesn't need 
to support all options though.</p><p>A compliant decompressor must be 
able to decompress at least one working set of parameters that conforms 
to the specifications presented here. It may also ignore checksums. 
Whenever it does not support a specific parameter within the compressed 
stream, it must produce a non-ambiguous error code and associated error 
message explaining which parameter is unsupported.</p><h2><a class="h" name="General-Structure-of-LZ4-Frame-format" href="#General-Structure-of-LZ4-Frame-format"><span></span></a><a class="h" name="general-structure-of-lz4-frame-format" href="#general-structure-of-lz4-frame-format"><span></span></a>General Structure of LZ4 Frame format</h2><table><thead><tr><th align="center">MagicNb</th><th align="center">F. Descriptor</th><th>Block</th><th>(...)</th><th>EndMark</th><th>C. Checksum</th></tr></thead><tbody><tr><td align="center">4 bytes</td><td align="center">3-15 bytes</td><td></td><td></td><td>4 bytes</td><td>0-4 bytes</td></tr></tbody></table><p><strong>Magic Number</strong></p><p>4 Bytes, Little endian format. Value : 0x184D2204</p><p><strong>Frame Descriptor</strong></p><p>3 to 15 Bytes, to be detailed in its own paragraph, as it is the most important part of the spec.</p><p>The combined <em>Magic_Number</em> and <em>Frame_Descriptor</em> fields are sometimes called <em><strong>LZ4 Frame Header</strong></em>. Its size varies between 7 and 19 bytes.</p><p><strong>Data Blocks</strong></p><p>To be detailed in its own paragraph. That’s where compressed data is stored.</p><p><strong>EndMark</strong></p><p>The flow of blocks ends when the last data block is followed by the 32-bit value <code class="code">0x00000000</code>.</p><p><strong>Content Checksum</strong></p><p><em>Content_Checksum</em> verify that the full content has been decoded correctly. The content checksum is the result of <a href="https://github.com/Cyan4973/xxHash/blob/release/doc/xxhash_spec.md">xxHash-32 algorithm</a>
 digesting the original (decoded) data as input, and a seed of zero. 
Content checksum is only present when its associated flag is set in the 
frame descriptor. Content Checksum validates the result, that all blocks
 were fully transmitted in the correct order and without error, and also
 that the encoding/decoding process itself generated no distortion. Its 
usage is recommended.</p><p>The combined <em>EndMark</em> and <em>Content_Checksum</em> fields might sometimes be referred to as <em><strong>LZ4 Frame Footer</strong></em>. Its size varies between 4 and 8 bytes.</p><p><strong>Frame Concatenation</strong></p><p>In
 some circumstances, it may be preferable to append multiple frames, for
 example in order to add new data to an existing compressed file without
 re-framing it.</p><p>In such case, each frame has its own set of 
descriptor flags. Each frame is considered independent. The only 
relation between frames is their sequential order.</p><p>The ability to 
decode multiple concatenated frames within a single stream or file is 
left outside of this specification. As an example, the reference lz4 
command line utility behavior is to decode all concatenated frames in 
their sequential order.</p><h2><a class="h" name="Frame-Descriptor" href="#Frame-Descriptor"><span></span></a><a class="h" name="frame-descriptor" href="#frame-descriptor"><span></span></a>Frame Descriptor</h2><table><thead><tr><th>FLG</th><th>BD</th><th align="center">(Content Size)</th><th align="center">(Dictionary ID)</th><th>HC</th></tr></thead><tbody><tr><td>1 byte</td><td>1 byte</td><td align="center">0 - 8 bytes</td><td align="center">0 - 4 bytes</td><td>1 byte</td></tr></tbody></table><p>The descriptor uses a minimum of 3 bytes, and up to 15 bytes depending on optional parameters.</p><p><strong>FLG byte</strong></p><table><thead><tr><th>BitNb</th><th>7-6</th><th>5</th><th>4</th><th>3</th><th>2</th><th>1</th><th>0</th></tr></thead><tbody><tr><td>FieldName</td><td>Version</td><td>B.Indep</td><td>B.Checksum</td><td>C.Size</td><td>C.Checksum</td><td><em>Reserved</em></td><td>DictID</td></tr></tbody></table><p><strong>BD byte</strong></p><table><thead><tr><th>BitNb</th><th>7</th><th>6-5-4</th><th>3-2-1-0</th></tr></thead><tbody><tr><td>FieldName</td><td><em>Reserved</em></td><td>Block MaxSize</td><td><em>Reserved</em></td></tr></tbody></table><p>In the tables, bit 7 is highest bit, while bit 0 is lowest.</p><p><strong>Version Number</strong></p><p>2-bits field, must be set to <code class="code">01</code>. Any other value cannot be decoded by this version of the specification. Other version numbers will use different flag layouts.</p><p><strong>Block Independence flag</strong></p><p>If
 this flag is set to “1”, blocks are independent. If this flag is set to
 “0”, each block depends on previous ones (up to LZ4 window size, which 
is 64 KB). In such case, it’s necessary to decode all blocks in 
sequence.</p><p>Block dependency improves compression ratio, especially 
for small blocks. On the other hand, it makes random access or 
multi-threaded decoding impossible.</p><p><strong>Block checksum flag</strong></p><p>If
 this flag is set, each data block will be followed by a 4-bytes 
checksum, calculated by using the xxHash-32 algorithm on the raw 
(compressed) data block. The intention is to detect data corruption 
(storage or transmission errors) immediately, before decoding. Block 
checksum usage is optional.</p><p><strong>Content Size flag</strong></p><p>If
 this flag is set, the uncompressed size of data included within the 
frame will be present as an 8 bytes unsigned little endian value, after 
the flags. Content Size usage is optional.</p><p><strong>Content checksum flag</strong></p><p>If this flag is set, a 32-bits content checksum will be appended after the EndMark.</p><p><strong>Dictionary ID flag</strong></p><p>If this flag is set, a 4-bytes Dict-ID field will be present, after the descriptor flags and the Content Size.</p><p><strong>Block Maximum Size</strong></p><p>This
 information is useful to help the decoder allocate memory. Size here 
refers to the original (uncompressed) data size. Block Maximum Size is 
one value among the following table :</p><table><thead><tr><th>0</th><th>1</th><th>2</th><th>3</th><th>4</th><th>5</th><th>6</th><th>7</th></tr></thead><tbody><tr><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>64 KB</td><td>256 KB</td><td>1 MB</td><td>4 MB</td></tr></tbody></table><p>The
 decoder may refuse to allocate block sizes above any system-specific 
size. Unused values may be used in a future revision of the spec. A 
decoder conformant with the current version of the spec is only able to 
decode block sizes defined in this spec.</p><p><strong>Reserved bits</strong></p><p>Value of reserved bits <strong>must</strong>
 be 0 (zero). Reserved bit might be used in a future version of the 
specification, typically enabling new optional features. When this 
happens, a decoder respecting the current specification version shall 
not be able to decode such a frame.</p><p><strong>Content Size</strong></p><p>This
 is the original (uncompressed) size. This information is optional, and 
only present if the associated flag is set. Content size is provided 
using unsigned 8 Bytes, for a maximum of 16 Exabytes. Format is Little 
endian. This value is informational, typically for display or memory 
allocation. It can be skipped by a decoder, or used to validate content 
correctness.</p><p><strong>Dictionary ID</strong></p><p>Dict-ID is only 
present if the associated flag is set. It's an unsigned 32-bits value, 
stored using little-endian convention. A dictionary is useful to 
compress short input sequences. The compressor can take advantage of the
 dictionary context to encode the input in a more compact manner. It 
works as a kind of “known prefix” which is used by both the compressor 
and the decompressor to “warm-up” reference tables.</p><p>The 
decompressor can use Dict-ID identifier to determine which dictionary 
must be used to correctly decode data. The compressor and the 
decompressor must use exactly the same dictionary. It's presumed that 
the 32-bits dictID uniquely identifies a dictionary.</p><p>Within a 
single frame, a single dictionary can be defined. When the frame 
descriptor defines independent blocks, each block will be initialized 
with the same dictionary. If the frame descriptor defines linked blocks,
 the dictionary will only be used once, at the beginning of the frame.</p><p><strong>Header Checksum</strong></p><p>One-byte checksum of combined descriptor fields, including optional ones. The value is the second byte of <code class="code">xxh32()</code> : <code class="code">(xxh32()&gt;&gt;8) &amp; 0xFF</code>
 using zero as a seed, and the full Frame Descriptor as an input 
(including optional fields when they are present). A wrong checksum 
indicates that the descriptor is erroneous.</p><h2><a class="h" name="Data-Blocks" href="#Data-Blocks"><span></span></a><a class="h" name="data-blocks" href="#data-blocks"><span></span></a>Data Blocks</h2><table><thead><tr><th align="center">Block Size</th><th>data</th><th align="center">(Block Checksum)</th></tr></thead><tbody><tr><td align="center">4 bytes</td><td></td><td align="center">0 - 4 bytes</td></tr></tbody></table><p><strong>Block Size</strong></p><p>This field uses 4-bytes, format is little-endian.</p><p>If the highest bit is set (<code class="code">1</code>), the block is uncompressed.</p><p>If the highest bit is not set (<code class="code">0</code>), the block is LZ4-compressed, using the <a href="https://github.com/lz4/lz4/blob/dev/doc/lz4_Block_format.md">LZ4 block format specification</a>.</p><p>All other bits give the size, in bytes, of the data section. The size does not include the block checksum if present.</p><p><em>Block_Size</em> shall never be larger than <em>Block_Maximum_Size</em>.
 Such an outcome could potentially happen for non-compressible sources. 
In such a case, such data block must be passed using uncompressed 
format.</p><p>A value of <code class="code">0x00000000</code> is invalid, and signifies an <em>EndMark</em> instead. Note that this is different from a value of <code class="code">0x80000000</code>
 (highest bit set), which is an uncompressed block of size 0 (empty), 
which is valid, and therefore doesn't end a frame. Note that, if <em>Block_checksum</em> is enabled, even an empty block must be followed by a 32-bit block checksum.</p><p><strong>Data</strong></p><p>Where the actual data to decode stands. It might be compressed or not, depending on previous field indications.</p><p>When compressed, the data must respect the <a href="https://github.com/lz4/lz4/blob/dev/doc/lz4_Block_format.md">LZ4 block format specification</a>.</p><p>Note that a block is not necessarily full. Uncompressed size of data can be any size <strong>up to</strong> <em>Block_Maximum_Size</em>, so it may contain less data than the maximum block size.</p><p><strong>Block checksum</strong></p><p>Only present if the associated flag is set. This is a 4-bytes checksum value, in little endian format, calculated by using the <a href="https://github.com/Cyan4973/xxHash/blob/release/doc/xxhash_spec.md">xxHash-32 algorithm</a> on the <strong>raw</strong>
 (undecoded) data block, and a seed of zero. The intention is to detect 
data corruption (storage or transmission errors) before decoding.</p><p><em>Block_checksum</em> can be cumulative with <em>Content_checksum</em>.</p><h2><a class="h" name="Skippable-Frames" href="#Skippable-Frames"><span></span></a><a class="h" name="skippable-frames" href="#skippable-frames"><span></span></a>Skippable Frames</h2><table><thead><tr><th align="center">Magic Number</th><th align="center">Frame Size</th><th>User Data</th></tr></thead><tbody><tr><td align="center">4 bytes</td><td align="center">4 bytes</td><td></td></tr></tbody></table><p>Skippable
 frames allow the integration of user-defined data into a flow of 
concatenated frames. Its design is pretty straightforward, with the sole
 objective to allow the decoder to quickly skip over user-defined data 
and continue decoding.</p><p>For the purpose of facilitating 
identification, it is discouraged to start a flow of concatenated frames
 with a skippable frame. If there is a need to start such a flow with 
some user data encapsulated into a skippable frame, it’s recommended to 
start with a zero-byte LZ4 frame followed by a skippable frame. This 
will make it easier for file type identifiers.</p><p><strong>Magic Number</strong></p><p>4
 Bytes, Little endian format. Value : 0x184D2A5X, which means any value 
from 0x184D2A50 to 0x184D2A5F. All 16 values are valid to identify a 
skippable frame.</p><p><strong>Frame Size</strong></p><p>This is the 
size, in bytes, of the following User Data (without including the magic 
number nor the size field itself). 4 Bytes, Little endian format, 
unsigned 32-bits. This means User Data can’t be bigger than (2^32-1) 
Bytes.</p><p><strong>User Data</strong></p><p>User Data can be anything. Data will just be skipped by the decoder.</p><h2><a class="h" name="Legacy-frame" href="#Legacy-frame"><span></span></a><a class="h" name="legacy-frame" href="#legacy-frame"><span></span></a>Legacy frame</h2><p>The
 Legacy frame format was defined into the initial versions of “LZ4Demo”.
 Newer compressors should not use this format anymore, as it is too 
restrictive.</p><p>Main characteristics of the legacy format :</p><ul><li>Fixed block size : 8 MB.</li><li>All blocks must be completely filled, except the last one.</li><li>All blocks are always compressed, even when compression is detrimental.</li><li>The
 last block is detected either because it is followed by the “EOF” (End 
of File) mark, or because it is followed by a known Frame Magic Number.</li><li>No checksum</li><li>Convention is Little endian</li></ul><table><thead><tr><th>MagicNb</th><th>B.CSize</th><th>CData</th><th>B.CSize</th><th>CData</th><th>(...)</th><th>EndMark</th></tr></thead><tbody><tr><td>4 bytes</td><td>4 bytes</td><td>CSize</td><td>4 bytes</td><td>CSize</td><td>x times</td><td>EOF</td></tr></tbody></table><p><strong>Magic Number</strong></p><p>4 Bytes, Little endian format. Value : 0x184C2102</p><p><strong>Block Compressed Size</strong></p><p>This is the size, in bytes, of the following compressed data block. 4 Bytes, Little endian format.</p><p><strong>Data</strong></p><p>Where the actual compressed data stands. Data is always compressed, even when compression is detrimental.</p><p><strong>EndMark</strong></p><p>End
 of legacy frame is implicit only. It must be followed by a standard EOF
 (End Of File) signal, whether it is a file or a stream.</p><p>Alternatively,
 if the frame is followed by a valid Frame Magic Number, it is 
considered completed. This policy makes it possible to concatenate 
legacy frames.</p><p>Any other value will be interpreted as a block size, and trigger an error if it does not fit within acceptable range.</p><h2><a class="h" name="Version-changes" href="#Version-changes"><span></span></a><a class="h" name="version-changes" href="#version-changes"><span></span></a>Version changes</h2><p>1.6.2 : clarifies specification of <em>EndMark</em></p><p>1.6.1 : introduced terms “LZ4 Frame Header” and “LZ4 Frame Footer”</p><p>1.6.0 : restored Dictionary ID field in Frame header</p><p>1.5.1 : changed document format to MarkDown</p><p>1.5 : removed Dictionary ID from specification</p><p>1.4.1 : changed wording from “stream” to “frame”</p><p>1.4 : added skippable streams, re-added stream checksum</p><p>1.3 : modified header checksum</p><p>1.2 : reduced choice of “block size”, to postpone decision on “dynamic size of BlockSize Field”.</p><p>1.1 : optional fields are now part of the descriptor</p><p>1.0 : changed “block size” specification, adding a compressed/uncompressed flag</p><p>0.9 : reduced scale of “block maximum size” table</p><p>0.8 : removed : high compression flag</p><p>0.7 : removed : stream checksum</p><p>0.6 : settled : stream size uses 8 bytes, endian convention is little endian</p><p>0.5: added copyright notice</p><p>0.4 : changed format to Google Doc compatible OpenDocument</p></div></div></div><!-- default customFooter --><footer class="Site-footer"><div class="Footer"><span class="Footer-poweredBy">Powered by <a href="https://gerrit.googlesource.com/gitiles/">Gitiles</a>| <a href="https://policies.google.com/privacy">Privacy</a>| <a href="https://policies.google.com/terms">Terms</a></span><div class="Footer-links"></div></div></footer></body></html>