% task, task_num 
task(generate_words, 1).
task(generate_words, 2).
task(count_words, 1).
task(count_words, 2).
task(aggregate_counts, 1).
% partition_in task, task_num, partition
partition_in(count_words, 1, 1).
partition_in(count_words, 2, 2).
partition_in(aggregate_counts, 3).
partition_in(aggregate_counts, 4).
partition_out(generate_words, 1, 1).
partition_out(generate_words, 2, 2).
partition_out(count_words, 1, 3).
partition_out(count_words, 2, 4).
partition_out(aggregate_counts, 5).
% a task is ready when all the partitions are available
% TODO: how is a task marked as done?
task_ready(Task, TaskNum, PartitionsReady) :- 
    task(Task, TaskNum), 
    forall(partition_in(Task, TaskNum, P),
           member(P, PartitionsReady)).
