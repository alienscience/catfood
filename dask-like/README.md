
# Simple Distributed Jobs

Entry point is in lib main::plan().

```rust
fn plan() {
    // Generate words put result into partition 1
    task_add!(tasks, worker::generate_words, &[], &[1]);
    // Generate words put result into partition 2
    task_add!(tasks, worker::generate_words, &[], &[2]);

    // Word count on partitions 1 & 2
    task_add!(tasks, worker::count_words, &[1], &[3]);
    task_add!(tasks, worker::count_words, &[2], &[4]);

    // Aggregate the word counts
    task_add!(tasks, worker::aggregate_counts, &[3, 4], &[5]);

    // Print the results in the main process
    task_add!(tasks, main::print_results, &[5], &[])
}
```

Worker code is in lib worker::xxx()

```rust
fn generate_words() -> Vec<Vec<u8>> {
    // Generate a single partition of random words
}
```