

# Entry Points

```go
/// Main entry point on Driver
func EntryPoint(env Environment) error {
    // Setup cluster computation
}

/// User defined entry point on a Worker
func DoStuff(env Environment) error {
    // Do computation
}
```

## Word Count

As explicit:
```go
func EntryPoint(env Environment) error {
    for partition := range(0, 8) {
        workerNumber := partition % 4
        // workerNumber, entryPoint, inputPartitions, outputPartitions
        env.Call(workerNumber, "GenerateWords", nil, &[]int{partition})
    }
    for inputPartition := range(0, 8) {
        workerNumber := 4 + (partition % 4)
        outputPartition := 8 + inputPartition
        env.Call(4, "CountWords", &[]int{inputPartition}, &[]int{outputPartition})
    }
    aggregatePartitions := make([]int, 8)
    for i := range(0, 8) {
        aggregatePartitions[i] = i + 8
    }
    env.Call(0, "AggregateCounts", aggregatePartitions, &[]int{16}})
    return nil
}

func GenerateWords(env Environment) error {
    words := generateWords()
    env.Result(words)
    return nil
}

func CountWords(env Environment) error {
    words := env.NextArg()
    counts := countWords(words)
    env.Result(counts)
    return nil
}

func AggregateCounts(env Environment) error {
    finalCounts := initialiseCounts()
    for env.HasNextArg() {
        counts := env.NextArg()
        updateCounts(&finalCounts, counts)
    }
    env.Result(finalCounts)
    return nil
}
```

As graphlike DSL with dynamic scheduling and reflection:

```go
func EntryPoint(env Environment) error {
    // words represents all output partitions
    words := env.Group(GenerateWords, env.Repeat(8))
    counts := env.Group(CountWords, env.Arg(words))
    finalCounts := env.Group(AggregateWords, env.Args(counts, 8))
    return nil
}

func CountWords(env Environment) error {
    words := env.NextArg()
    counts := countWords(words)
    env.Result(counts)
    return nil
}
```
