
# Super Simple Reactive Streams

Based on the [simple reactive streams](https://github.com/bassemZohdy/simple-reactive-streams) java
implementation and attempts to follow the [reactive streams specification](https://github.com/reactive-streams/reactive-streams-jvm/blob/v1.0.3/README.md#specification).

- No async runtime required.
- Minimal number of combinators - just convert a stream to an iterator with `.iter()` and use the combinators provided by `std::iter`.

# TODO

- Threading/executor
- Add layer for Flux