
# Overview

```mermaid
classDiagram
class Publisher~Item~
class Subscriber~Item~
class Subscription


<<trait>> Publisher
Publisher : subscribe(s Subscriber~Item~)

<<trait>> Subscriber
Subscriber : on_subscribe(s Subscription)
Subscriber : on_next(item Item)
Subscriber : on_error(err Error)
Subscriber : on_complete()

<<trait>> Subscription
Subscription : request(n u32)
Subscription : cancel()

Publisher "1" --> "*" Subscription : creates
Subscriber "1" --> "1" Subscription : controls
Publisher "1" --> "*" Subscriber : calls
```