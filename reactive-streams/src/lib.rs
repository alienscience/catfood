mod implementation;
pub mod spec;

pub use crate::implementation::{SimplePublisher, SimpleSubscriber, SimpleSubscription};

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
