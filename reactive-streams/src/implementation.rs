use std::sync::{
    atomic::{AtomicBool, AtomicU32, Ordering},
    Arc,
};

use crate::spec::{Publisher, Subscriber, Subscription};

pub struct SimplePublisher<T> {
    supplier: Box<dyn FnMut() -> Option<T>>,
}

impl<T> SimplePublisher<T> {
    pub fn of<S>(s: S) -> Self
    where
        S: FnMut() -> Option<T> + 'static,
    {
        Self {
            supplier: Box::new(s),
        }
    }
}

impl<T: 'static> Publisher for SimplePublisher<T> {
    type Item = T;

    fn subscribe(&mut self, s: Box<dyn Subscriber<Item = Self::Item>>) {
        let subscription = SimpleSubscription::new();
        let mut subscriber = s;
        subscriber.on_subscribe(Box::new(subscription.clone()));
        // TODO: threading
        // TODO: errors with on_error()
        'subscription: while !subscription.inner.is_cancelled() {
            for _ in 0..subscription.inner.demand() {
                if let Some(v) = (self.supplier)() {
                    subscriber.on_next(v);
                } else {
                    break 'subscription;
                }
            }
        }
        subscriber.on_complete();
    }
}

struct InnerSubscription {
    demand: AtomicU32,
    cancelled: AtomicBool,
}

impl InnerSubscription {
    fn request(&self, n: u32) {
        self.demand.fetch_add(n, Ordering::Relaxed);
    }

    fn demand(&self) -> u32 {
        self.demand.load(Ordering::Relaxed)
    }

    fn cancel(&self) {
        self.cancelled.store(true, Ordering::Relaxed)
    }

    fn is_cancelled(&self) -> bool {
        self.cancelled.load(Ordering::Relaxed)
    }
}

// TODO: share subscription between SimplePublisher and SimpleSubscriber using Arc
// TODO: maybe make trait methods non-mut
#[derive(Clone)]
pub struct SimpleSubscription {
    inner: Arc<InnerSubscription>,
}

impl SimpleSubscription {
    fn new() -> Self {
        Self {
            inner: Arc::new(InnerSubscription {
                demand: AtomicU32::new(0),
                cancelled: AtomicBool::new(false),
            }),
        }
    }
}

impl Subscription for SimpleSubscription {
    fn request(&mut self, n: u32) {
        self.inner.request(n);
    }

    fn cancel(&mut self) {
        self.inner.cancel()
    }
}

pub struct SimpleSubscriber<T> {
    subscription: Option<Box<dyn Subscription>>,
    consumer: Box<dyn FnMut(T)>,
}

impl<T> SimpleSubscriber<T> {
    pub fn with<S>(s: S) -> Self
    where
        S: FnMut(T) + 'static,
    {
        Self {
            subscription: None,
            consumer: Box::new(s),
        }
    }
}

impl<T> Subscriber for SimpleSubscriber<T> {
    type Item = T;

    fn on_subscribe(&mut self, s: Box<dyn Subscription>) {
        self.subscription = Some(s);
        if let Some(s) = &mut self.subscription {
            s.request(1);
        }
    }

    fn on_next(&mut self, item: Self::Item) {
        (self.consumer)(item);
        if let Some(s) = &mut self.subscription {
            s.request(1);
        }
    }

    fn on_error(&mut self, _err: Box<dyn std::error::Error>) {
        todo!()
    }

    fn on_complete(&mut self) {
        // OK
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn counter() {
        const MAX_COUNT: u32 = 10;
        let mut count = 0;
        let mut producer = SimplePublisher::of(move || {
            if count > MAX_COUNT {
                return None;
            }
            let ret = count;
            count += 1;
            Some(ret)
        });
        let received = Arc::new(AtomicU32::new(0));
        let subscription_received = received.clone();
        let subscription = SimpleSubscriber::with(move |i| {
            subscription_received.store(i, Ordering::Relaxed);
        });
        producer.subscribe(Box::new(subscription));
        let received = received.load(Ordering::Relaxed);
        assert_eq!(received, MAX_COUNT);
    }
}
