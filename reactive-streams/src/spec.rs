pub trait Publisher {
    type Item;

    fn subscribe(&mut self, s: Box<dyn Subscriber<Item = Self::Item>>);
}

pub trait Subscriber {
    type Item;

    // TODO: Subscription should be Arc
    fn on_subscribe(&mut self, s: Box<dyn Subscription>);
    fn on_next(&mut self, item: Self::Item);
    fn on_error(&mut self, err: Box<dyn std::error::Error>);
    fn on_complete(&mut self);
}

pub trait Subscription {
    fn request(&mut self, n: u32);
    fn cancel(&mut self);
}

pub trait Processor<InItem, OutItem>:
    Subscriber<Item = InItem> + Publisher<Item = OutItem>
{
}
